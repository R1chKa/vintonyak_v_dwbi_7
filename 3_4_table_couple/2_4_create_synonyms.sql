USE education
GO

-- Schema creating
CREATE SCHEMA [V_Vintonyak]
GO

-- Synonyms creating
CREATE SYNONYM [V_Vintonyak].shipment FOR V_V_module_3.dbo.[shipment]
GO
CREATE SYNONYM [V_Vintonyak].shipment_log FOR V_V_module_3.dbo.[shipment_log]
GO

-- Check the result
SELECT * FROM [V_Vintonyak].shipment
SELECT * FROM [V_Vintonyak].shipment_log