USE V_V_module_3
GO

-- Views creation
CREATE OR ALTER VIEW vw_customers
(
	id, 
	name, 
	surname, 
	age, 
	city, 
	country, 
	phone_num,
	inserted_date,
	updated_date,
	orders_id
)
AS
SELECT * FROM customers
GO

CREATE OR ALTER VIEW vw_orders
(
	id, 
	product, 
	price, 
	amount, 
	discount, 
	type, 
	status, 
	comments,
	inserted_date,
	updated_date
)
AS
SELECT * FROM orders
GO

-- Check the result
SELECT * FROM vw_customers
SELECT * FROM vw_orders