USE V_V_module_3
GO

--  Inserting data into table shipment and testing it
INSERT INTO [shipment]
		([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
		[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
		[comment])
VALUES (1, 1, 1, 'letter', 5, 'visa', '14th Street NW', '', 'Washington', 'Washington', 
		'20004', 'USA', 10.00, 'in proggress', '');
INSERT INTO [shipment]
		([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
		[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
		[comment])
VALUES (2, 2, 2, 'box', 9, 'paypal', 'W 19th Street', '', 'Arizona', 'Arizona', 
		'85344', 'USA', 20.00, 'in proggress', '');
INSERT INTO [shipment]
		([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
		[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
		[comment])
VALUES (4, 3, 3, 'box', 9, 'paypal', 'W 19th Street', '', 'Arizona', 'Arizona', 
		'85344', 'USA', 20.00, 'in proggress', '');

-- DELETE testing
DELETE FROM [shipment] WHERE id = 4

-- UPDATE testing
UPDATE [shipment] SET [shipment_type] = '12345', [duration] = 6, [payment_type] = '12345', [street1] = '12345', [street2] = '12345', [city] = '12345', [state] = '12345', [postal_code] = '12345', [country] = '12345', [price] = 15, [status] = '12345', [comment] = '12345' FROM [shipment] WHERE id = 1
UPDATE [shipment] SET [shipment_type] = 'AAAAA', [duration] = 1, [payment_type] = 'AAAAA', [street1] = 'AAAAA', [street2] = 'AAAAA', [city] = 'AAAAA', [state] = 'AAAAA', [postal_code] = 'AAAAA', [country] = 'AAAAA', [price] = 5, [status] = 'AAAAA', [comment] = 'AAAAA' FROM [shipment] WHERE id = 1

-- Check the result
SELECT * FROM shipment
SELECT * FROM shipment_log