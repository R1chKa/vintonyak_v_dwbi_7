USE V_V_module_3
GO

CREATE TABLE  customers (	
    id INT NOT NULL PRIMARY KEY, 
	name VARCHAR(40) NOT NULL, 
	surname VARCHAR(40) NOT NULL UNIQUE,
	age INT NOT NULL CHECK(age >= 16),
	city VARCHAR(40),
	country VARCHAR(20) NOT NULL,  
	phone_num VARCHAR(17) NOT NULL UNIQUE,
	inserted_date DATETIME NOT NULL DEFAULT GETDATE(),
	updated_date DATETIME NOT NULL DEFAULT GETDATE(),
	orders_id INT
)

CREATE TABLE  orders (	
    id INT NOT NULL PRIMARY KEY, 
	product VARCHAR(40) NOT NULL, 
	price DECIMAL(6,2) NOT NULL,
	amount INT NOT NULL,
	discount INT CHECK(discount <= 50),
	type VARCHAR(40),
	status VARCHAR(40) NOT NULL, 
	comments VARCHAR(40),
	inserted_date DATETIME NOT NULL DEFAULT GETDATE(),
	updated_date DATETIME NOT NULL DEFAULT GETDATE()
)

ALTER TABLE customers
	ADD CONSTRAINT FK_Customers_Orders
	FOREIGN KEY	(orders_id) REFERENCES orders (id)