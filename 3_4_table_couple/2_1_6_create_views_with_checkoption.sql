USE V_V_module_3
GO

-- Views creation
CREATE OR ALTER VIEW vw_customersCO
(
	id, 
	name, 
	surname, 
	age, 
	city, 
	country, 
	phone_num,
	inserted_date,
	updated_date,
	orders_id
)
AS
SELECT * 
	FROM customers 
	WHERE age >= 20
	WITH CHECK OPTION
GO

-- Check the result
SELECT * FROM vw_customersCO