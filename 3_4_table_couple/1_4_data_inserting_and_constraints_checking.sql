USE V_V_module_3
GO

--  Test FK_Customers_Orders constraint --> Error, because no data in FK table yet
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (1, 'Bob', 'Fox', 22, 'London', 'England', '(020) 1234 5678',  1);

--  Inserting data into table orders
INSERT INTO orders (id, product, price, amount, discount, type, status, comments) 
VALUES (1, 'Bike', '220', 1, 10, 'sport', 'with shipment', 'finished');
INSERT INTO orders (id, product, price, amount, discount, type, status, comments) 
VALUES (2, 'Notebook', '300', 1, 5, 'office', 'without shipment', 'in progress');
INSERT INTO orders (id, product, price, amount, discount, type, status, comments) 
VALUES (3, 'Sushi', '20', 10, 1, 'japaniese', 'with shipment', 'finished');

--  Inserting data into table customers
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (1, 'Bob', 'Fox', 22, 'London', 'England', '(020) 1234 5678', 1);
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (2, 'David', 'Barret', 17, 'Lviv', 'Ukraine', '(380) 1234 5678', 2);
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (3, 'Janette', 'Cheshire', 19, 'Tokyo', 'Japan', '(333) 1234 5678', 3);

-- Check inserted data
SELECT * FROM customers
GO

SELECT * FROM orders
GO

-- Check Check-constrains --> Error because inserting false data in age column
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (5, 'John', 'Connor', 15, 'Ivano-Frankivsk', 'Ukraine', '(380) 8765 4321', 2);
-- Check Check-constrains --> Error because inserting false data in discount column
INSERT INTO orders (id, product, price, amount, discount, type, status, comments) 
VALUES (4, 'Bike', '220', 1, 51, 'sport', 'with shipment', 'finished');

-- Check Unique-constrains --> Error because inserting not unique data into surname column
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (4, 'John', 'Fox', 22, 'Ivano-Frankivsk', 'Ukraine', '(380) 8765 4321', 2);
-- Check Unique constrains --> Error because inserting not unique data into phone_num column
INSERT INTO customers (id, name, surname, age, city, country, phone_num, orders_id) 
VALUES (5, 'John', 'Connor', 22, 'Ivano-Frankivsk', 'Ukraine', '(380) 1234 5678', 2);
