USE V_V_module_3
GO

DROP TRIGGER IF EXISTS tr_customersU
GO
DROP TRIGGER IF EXISTS tr_ordersU
GO

-- Triggers creation
CREATE TRIGGER tr_customersU on customers
AFTER UPDATE
AS
BEGIN
	UPDATE customers
	SET customers.updated_date = GETDATE()
	FROM customers
	INNER JOIN inserted 
		ON customers.id = inserted.id
END
GO

CREATE TRIGGER tr_ordersU on orders
AFTER UPDATE
AS
BEGIN
	UPDATE orders
	SET orders.updated_date = GETDATE()
	FROM orders
	INNER JOIN inserted 
		ON orders.id = orders.id
END
GO

-- Update some data in both tables
UPDATE customers SET name = 'asxd' WHERE id = 3
UPDATE orders SET product = 'asxd' WHERE id = 3

-- Check the result after update
SELECT * FROM customers
GO
SELECT * FROM orders
GO