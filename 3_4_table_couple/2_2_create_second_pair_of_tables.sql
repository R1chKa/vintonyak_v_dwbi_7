USE V_V_module_3
GO

DROP TABLE IF EXISTS [shipment]
GO
DROP TABLE IF EXISTS [shipment_log]
GO
-- Table creati[on
CREATE TABLE  [shipment] (
    [id] INT NOT NULL PRIMARY KEY,
	[customer_id] INT NOT NULL,
	[order_id] INT NOT NULL,
	[shipment_type] VARCHAR(20) NOT NULL,
	[duration] INT NOT NULL,
	[payment_type] VARCHAR(20) NOT NULL,
	[street1] VARCHAR(40) NOT NULL,
	[street2] VARCHAR(40),
	[city] VARCHAR(20) NOT NULL,
	[state] VARCHAR(20),
	[postal_code] VARCHAR(10) NOT NULL,
	[country] VARCHAR(20) NOT NULL,
	[price] DECIMAL(6,2) NOT NULL,
	[status] VARCHAR(40),
	[comment] VARCHAR(40)
)

CREATE TABLE  [shipment_log] (
    [id] INT NOT NULL,
	[customer_id] INT NOT NULL,
	[order_id] INT NOT NULL,
	[shipment_type] VARCHAR(40) NOT NULL,
	[duration] VARCHAR(40) NOT NULL,
	[payment_type] VARCHAR(40) NOT NULL,
	[street1] VARCHAR(80) NOT NULL,
	[street2] VARCHAR(80),
	[city] VARCHAR(40) NOT NULL,
	[state] VARCHAR(40),
	[postal_code] VARCHAR(20) NOT NULL,
	[country] VARCHAR(40) NOT NULL,
	[price] VARCHAR(80) NOT NULL,
	[status] VARCHAR(80),
	[comment] VARCHAR(80),
	[operation_type] CHAR(1) NOT NULL,
	[operation_date] DATETIME NOT NULL DEFAULT GETDATE()
)