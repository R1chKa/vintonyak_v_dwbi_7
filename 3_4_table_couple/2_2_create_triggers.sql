USE V_V_module_3
GO

DROP TRIGGER IF EXISTS tr_shipmentIUD 
GO

---- trigger creation
CREATE TRIGGER tr_shipmentIUD ON shipment
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	--SET NOCOUNT ON;  set @@rowcount in 0
	IF @@ROWCOUNT = 0 RETURN
		DECLARE @operation CHAR(1)
		DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
		DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

	SET @operation = 
	CASE	 
		WHEN @ins > 0 AND @del = 0 THEN 'I'
		WHEN @ins > 0 AND @del > 0 THEN 'U'
		WHEN @ins = 0 AND @del > 0 THEN 'D'
	END

	-- insert
	IF @operation = 'I'
	BEGIN
		INSERT INTO shipment_log 
			([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
			[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
			[comment], [operation_type])
		SELECT 
			inserted.[id], inserted.[customer_id], inserted.[order_id], inserted.[shipment_type], 
			inserted.[duration], inserted.[payment_type], inserted.[street1], inserted.[street2], 
			inserted.[city], inserted.[state], inserted.[postal_code], inserted.[country], inserted.[price], 
			inserted.[status], inserted.[comment], @operation
		FROM shipment 
			INNER JOIN inserted ON shipment.id = inserted.id
	END

	-- update
	IF @operation = 'U'
		BEGIN
	--SELECT power(2, (4-1)) + power(2, (5-1)) + power(2, (6-1)) + power(2, (7-1)) + power(2, (8-1)) + power(2, (9-1)) + power(2, (10-1)) + power(2, (11-1)) + power(2, (12-1)) + power(2, (13-1)) + power(2, (14-1)) + power(2, (15-1)) === 32760
		IF (COLUMNS_UPDATED() & 32760) > 0
			INSERT INTO shipment_log 
				([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
				[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
				[comment], [operation_type])
			SELECT 
				deleted.[id], deleted.[customer_id], deleted.[order_id], deleted.[shipment_type] + ' --> ' + inserted.[shipment_type], 
				CONVERT(VARCHAR(20), deleted.[duration]) + ' --> ' + CONVERT(VARCHAR(20), inserted.[duration]), deleted.[payment_type] + ' --> ' + inserted.[payment_type], 
				deleted.[street1] + ' --> ' + inserted.[street1], deleted.[street2] + ' --> ' + inserted.[street2], 
				deleted.[city] + ' --> ' + inserted.[city], deleted.[state] + ' --> ' + inserted.[state], 
				deleted.[postal_code] + ' --> ' + inserted.[postal_code], deleted.[country] + ' --> ' + inserted.[country], 
				CONVERT(VARCHAR(20), deleted.[price]) + ' --> ' + CONVERT(VARCHAR(20), inserted.[price]), deleted.[status] + ' --> ' + inserted.[status],
				deleted.[comment] + ' --> ' + inserted.[comment], @operation
			FROM shipment
				INNER JOIN  inserted ON shipment.id = inserted.id
				INNER JOIN deleted ON shipment.id = deleted.id
	END

	-- delete
	IF @operation = 'D'
	BEGIN
		INSERT INTO shipment_log 
			([id], [customer_id], [order_id], [shipment_type], [duration], [payment_type], 
			[street1], [street2], [city], [state], [postal_code], [country], [price], [status], 
			[comment], [operation_type])
		SELECT 
			deleted.[id], deleted.[customer_id], deleted.[order_id], deleted.[shipment_type], 
			deleted.[duration], deleted.[payment_type], deleted.[street1], deleted.[street2], 
			deleted.[city], deleted.[state], deleted.[postal_code], deleted.[country], deleted.[price], 
			deleted.[status], deleted.[comment], @operation
		FROM deleted 
	END
END
GO