USE education
GO

SELECT [id], [shipment_type], [duration], [price] 
FROM [V_Vintonyak].shipment

SELECT [id], [street1], [street2], [city], [state], [postal_code], [country] 
FROM [V_Vintonyak].shipment

SELECT [id], [customer_id], [order_id], [operation_type], [operation_date] 
FROM [V_Vintonyak].shipment_log

SELECT * 
FROM [V_Vintonyak].shipment_log 
WHERE [operation_type] = 'U'
