use [labor_sql];
go

--1
SELECT maker, type 
FROM product 
WHERE type = 'laptop' 
ORDER BY maker

--2
SELECT model, ram, screen, price 
FROM laptop
WHERE price > 1000 
ORDER BY ram ASC, price DESC

--3
SELECT *
FROM printer
WHERE color = 'y'
ORDER BY price DESC

--4
SELECT model, speed, hd, cd, price 
FROM pc
WHERE cd in ('12x', '24x') AND price < 600
ORDER BY speed DESC

--5
SELECT name, class
FROM ships
WHERE name = class
ORDER BY name 

--6
SELECT *
FROM pc
WHERE speed !< 500 AND price < 800
ORDER BY price DESC

--7
SELECT *
FROM printer
WHERE type != 'Matrix' AND price < 300
ORDER BY type DESC

--8
SELECT model, speed
FROM pc
WHERE price >= 400 AND price <= 600
ORDER BY hd

--9
SELECT model, speed, hd, price
FROM laptop
WHERE screen !< 12
ORDER BY price DESC

--10
SELECT model, type, price
FROM printer
WHERE price < 300
ORDER BY type DESC

--11
SELECT model, ram, price 
FROM laptop 
WHERE ram = 64
ORDER BY screen

--12
SELECT model, ram, price 
FROM pc
WHERE ram > 64
ORDER BY hd

--13
SELECT model, speed, price 
FROM pc
WHERE speed >= 500 AND speed <= 750
ORDER BY hd DESC

--14
SELECT *
FROM outcome_o
WHERE out > 2000
ORDER BY date DESC

--15
SELECT *
FROM income_o
WHERE inc >= 5000 AND inc <= 10000
ORDER BY inc

--16
SELECT *
FROM income
WHERE point = 1
ORDER BY inc

--17
SELECT *
FROM outcome
WHERE point = 2
ORDER BY out

--18
SELECT *
FROM classes
WHERE country = 'Japan'
ORDER BY type DESC

--19
SELECT name, launched
FROM ships
WHERE launched BETWEEN 1920 AND 1942
ORDER BY launched DESC

--20
SELECT ship, battle, result
FROM outcomes
WHERE battle = 'Guadalcanal' AND result != 'sunk'
ORDER BY ship DESC

--21
SELECT ship, battle, result
FROM outcomes 
WHERE result = 'sunk'
ORDER BY ship DESC

--22
SELECT class, displacement
FROM classes
WHERE displacement !< 40000
ORDER BY type

--23
SELECT trip_no, town_from, town_to
FROM trip
WHERE town_from = 'London' OR town_to = 'London'
ORDER BY time_out

--24
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane = 'TU-134'
ORDER BY time_out DESC

--25
SELECT trip_no, plane, town_from, town_to
FROM trip
WHERE plane != 'IL-86'
ORDER BY plane 

--26
SELECT trip_no, town_from, town_to
FROM trip
WHERE town_from != 'Rostov' AND town_to != 'Rostov'
ORDER BY plane

--27
SELECT DISTINCT model
FROM pc
WHERE model LIKE '%11%'
select * from pc
where (select len(model) - len(replace(model,'1',''))) >= 2
go

--28
SELECT *
FROM outcome
WHERE MONTH(date) = 3

--29
SELECT *
FROM outcome_o
WHERE DAY(date) = 14

--30
SELECT name
FROM ships
WHERE name LIKE 'W%n'

--31
SELECT name
FROM ships
WHERE LEN(name) - LEN(REPLACE(name, 'e', '')) = 2

--32
SELECT name, launched
FROM ships
WHERE name NOT LIKE '%a'

--33
SELECT name
FROM battles
WHERE name LIKE '% %' AND name NOT LIKE '%c'

--34
SELECT *
FROM trip
WHERE time_out BETWEEN '12:00:00' AND '17:00:00'

--35
SELECT *
FROM trip
WHERE time_in BETWEEN '17:00:00' AND '23:00:00'

--36
SELECT *
FROM trip
WHERE (DATEPART(HOUR, time_in) BETWEEN 21 AND 24) OR (DATEPART(HOUR, time_in) BETWEEN 00 AND 10)

--37
SELECT DISTINCT date
FROM pass_in_trip
WHERE place LIKE '1%'

--38
SELECT DISTINCT date
FROM pass_in_trip
WHERE place LIKE '%c'

--39
SELECT ((REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', ''))) as 'Surname'
FROM passenger
WHERE (REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', '')) LIKE 'C%'

--40
SELECT ((REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', ''))) as 'Surname'
FROM passenger
WHERE (REPLACE(SUBSTRING(name, CHARINDEX(' ', name), LEN(name)), ' ', '')) NOT LIKE 'J%'

--41
SELECT '������� ���� = ' + CONVERT(VARCHAR(11), AVG(price))
FROM laptop

--42
SELECT CONCAT('���: ', code), CONCAT('������: ', model), CONCAT('��������: ', speed), CONCAT('�ᒺ� �����: ', ram), CONCAT('�����
�����: ', hd), CONCAT('�������� CD-�������: ', cd), CONCAT('����: ', price)
FROM PC

--43
SELECT convert(varchar, date, 102)
FROM income

--44
SELECT ship, battle, result =
	CASE result
		WHEN 'OK' THEN '� �������'
		WHEN 'damaged' THEN '�����������'
		WHEN 'sunk' THEN '����������'
	END
FROM outcomes

--45
SELECT trip_no, date, id_psg, '���: ' + LEFT(place,1), '����: ' + SUBSTRING(place,2,1)
FROM pass_in_trip

--46
SELECT trip_no, id_comp, plane,time_out, time_in, CONCAT(' from ', TRIM(town_from), ' to ', town_to) AS 'info'
FROM trip

--47
SELECT CONCAT(LEFT(trip_no, 1), RIGHT(trip_no, 1), ' ', LEFT(id_comp, 1), RIGHT(id_comp, 1), ' ', LEFT(plane, 1), RIGHT(TRIM(plane), 1), ' ', LEFT(town_from, 1), RIGHT(TRIM(town_from), 1), ' ', LEFT(town_to, 1), RIGHT(TRIM(town_to), 1), ' ', LEFT(time_out, 1), RIGHT(time_out, 1), ' ', LEFT(time_in, 1), RIGHT(time_in, 1))
FROM trip

--48
SELECT maker, COUNT(model) 
FROM product
WHERE type = 'pc'
GROUP BY maker
HAVING COUNT(model) >= 2

--49
SELECT town, COUNT(trip_no)
FROM
(
	SELECT trip_no, town_from as town FROM trip
	UNION ALL
	SELECT trip_no, town_to as town FROM trip
) a
GROUP BY town

--50
SELECT type, COUNT(*)
FROM printer
GROUP BY type

--51
SELECT cd, model, count(DISTINCT cd) as cd_cnt, count(DISTINCT model) as models_cnt
FROM pc
GROUP BY GROUPING SETS(model, cd);

--52
SELECT trip_no, CAST((time_in-time_out) as time(0)) '[hh:mm:ss]'
FROM trip

--53
SELECT point, date, SUM(out), MIN(out), MAX(out)
FROM outcome
GROUP BY point, date WITH ROLLUP

--54
SELECT trip_no, LEFT(place, 1) AS '���', COUNT(*) AS 'ʳ������ ����'
FROM pass_in_trip
GROUP BY trip_no, LEFT(place,1)
ORDER BY trip_no

--55
SELECT CONCAT('ʳ������: ', COUNT(name)) as Count_name FROM passenger
WHERE name LIKE '% [S,B,A]%' 
