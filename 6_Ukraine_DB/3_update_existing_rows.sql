USE [people_ua_db]
GO

ALTER TABLE people ADD birth DATE
ALTER TABLE people ADD region NVARCHAR(30)

DECLARE @FromDate DATE = '1925-01-01',
		@ToDate DATE = '2000-01-01',
		@birth DATE
DECLARE @CurrentRow INT, @i INT

SET @CurrentRow = (SELECT COUNT(*) FROM people)
SET @i = 1

WHILE @i <= @CurrentRow
BEGIN
	SET @birth = (SELECT dateadd(day, rand(checksum(newid()))*(1+datediff(day, @FromDate, @ToDate)), @FromDate))

	UPDATE people
	SET birth = @birth
	FROM people
	WHERE id = @i 

	SET @i = @i + 1
END
GO

UPDATE people
SET region=a.region
FROM people
OUTER APPLY
(
SELECT TOP 1 region
	FROM 
	( 
		VALUES	('³������� �������'),
				('��������� �������'),
				('���������������� �������'),
				('�������� �������'),
				('����������� �������'),
				('������������ �������'),
				('��������� �������'),
				('�����-���������� �������'),
				('������� �������'),
				('ʳ������������ �������'),
				('��������� �������'),
				('�������� �������'),
				('����������� �������'),
				('������� �������'),
				('���������� �������'),
				('г�������� �������'),
				('������� �������'),
				('������������ �������'),
				('��������� �������'),
				('���������� �������'),
				('����������� �������'),
				('��������� �������'),
				('����������� �������'),
				('����������� �������')
	) r(region)
	WHERE people.id = people.id ORDER BY NEWID()
)a;

SELECT * FROM people