USE [people_ua_db]
GO

DECLARE @index_name AS NVARCHAR(50),
		@table_name AS NVARCHAR(50),
		@frag_percent AS FLOAT,
		@sql AS VARCHAR(max)

DECLARE auto_rebuild_or_reorganize CURSOR FOR
(
	SELECT sys_index.[name] AS index_name,
		object_name(sys_index_stats.[object_id]) AS table_name,
		sys_index_stats.[avg_fragmentation_in_percent] AS frag_percent
	FROM sys.dm_db_index_physical_stats(DB_ID('people_ua_db'), NULL, NULL, NULL, 'SAMPLED') AS sys_index_stats
		INNER JOIN sys.indexes AS sys_index  
		ON sys_index_stats.[object_id] = sys_index.[object_id] 
			AND sys_index_stats.[index_id]=sys_index.[index_id]
	WHERE object_name(sys_index_stats.[object_id])
		IN ('people')
)

OPEN auto_rebuild_or_reorganize;

	FETCH NEXT FROM auto_rebuild_or_reorganize INTO @index_name, @table_name, @frag_percent

	WHILE @@FETCH_STATUS=0
	BEGIN

		SET @sql='ALTER INDEX '+@index_name+' ON '+@table_name;

		IF @frag_percent>5 AND @frag_percent<30
		BEGIN
			EXEC(@sql+' REORGANIZE')
		END

		IF @frag_percent>=30
		BEGIN
			EXEC(@sql+' REBUILD')
		END

		FETCH NEXT FROM auto_rebuild_or_reorganize INTO @index_name, @table_name, @frag_percent

	END;

CLOSE auto_rebuild_or_reorganize;
DEALLOCATE auto_rebuild_or_reorganize;