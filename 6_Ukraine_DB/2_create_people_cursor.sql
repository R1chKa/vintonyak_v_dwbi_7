USE [people_ua_db]
GO

DROP TABLE IF EXISTS people
CREATE TABLE people (
	id			INT IDENTITY,
	surname		NVARCHAR(20),
	name		NVARCHAR(20),
	sex			NCHAR(1)
)

DROP TABLE IF EXISTS union_cte
CREATE TABLE union_cte (
		ids				INT,
		surname			NVARCHAR(20),
		sexs			NCHAR(1),
		amount			INT,
		idn				INT,
		name			NVARCHAR(20),
		sexn			NCHAR(1)
)

DECLARE @ids			INT,
		@surname		NVARCHAR(20),
		@sex			NCHAR(1),
		@amount			INT
DECLARE @Cycle INT, @Remainder INT, @i INT

DECLARE People_Cursor CURSOR
	FOR 
	SELECT id, surname, sex, amount
	FROM surname_list_identity
	
	;WITH People_CTE (ids, surname, sexs, amount, idn, name, sexn)
	AS
	(
		SELECT ids, surname, sexs, amount, idn, name, sexn
		FROM
		(
			SELECT s.id AS ids, s.surname, s.sex AS sexs, s.amount, n.id AS idn, n.name, n.sex AS sexn
			FROM [surname_list_identity] s
			JOIN [name_list_identity] n
			ON s.SEX=n.sex
			UNION
			SELECT s.id AS ids, s.surname, s.sex AS sexs, s.amount, n.id AS idn, n.name, n.sex AS sexn
			FROM [surname_list_identity] s
			JOIN [name_list_identity] n
			ON ISNULL(s.SEX,'w')=n.sex
			WHERE s.SEX IS NULL
			UNION
			SELECT s.id AS ids, s.surname, s.sex AS sexs, s.amount, n.id AS idn, n.name, n.sex AS sexn
			FROM [surname_list_identity] s
			JOIN [name_list_identity] n
			ON ISNULL(s.SEX,'m')=n.sex
			WHERE s.SEX IS NULL
		) AS tbl
	)
	INSERT INTO union_cte 
	SELECT ids, surname, sexs, amount, idn, name, sexn 
	FROM People_CTE

OPEN People_Cursor

	FETCH NEXT FROM People_Cursor 
	INTO @ids, @surname, @sex, @amount

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @amount > (SELECT COUNT(*) FROM name_list_identity)
		BEGIN
			SET @Cycle = ROUND((@amount / (SELECT COUNT(*) FROM name_list_identity)), 0, 1)
			SELECT @Cycle
			SET @i = 1

			WHILE @i < @Cycle
			BEGIN
				INSERT INTO people
				SELECT surname, name, sexn
				FROM union_cte
				WHERE surname = @surname
				OPTION (QUERYTRACEON 9495, MAXDOP 1) -- improve performance

				PRINT 'Inserted one cycle'
		
				SET @i = @i + 1
			END

			SET @Remainder =  @amount - (SELECT COUNT(*) FROM people WHERE surname = @surname)
			SET @i = 1

			WHILE @i < @Remainder
			BEGIN
				INSERT INTO people
				SELECT surname, name, sexn
				FROM union_cte
				WHERE surname = @surname AND idn = @i
				OPTION (QUERYTRACEON 9495, MAXDOP 1)

				PRINT 'Inserted reminder'
		
				SET @i = @i + 1
			END

			PRINT 'Inserted surname'
		END

		ELSE 
		BEGIN
			SET @i = 1
			WHILE @i < @amount
			BEGIN
				INSERT INTO people
				SELECT surname, name, sexn
				FROM union_cte
				WHERE surname = @surname AND idn = @i
				OPTION (QUERYTRACEON 9495, MAXDOP 1)

				PRINT 'Inserted reminder'
		
				SET @i = @i + 1
			END
		END

		FETCH NEXT FROM People_Cursor 
		INTO @ids, @surname, @sex, @amount
	END
CLOSE People_Cursor
DEALLOCATE People_Cursor