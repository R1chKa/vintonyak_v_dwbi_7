USE [people_ua_db]
GO

SELECT  sys_index.[name] AS [index],
		object_name(sys_index_stats.[object_id]) AS 'table',
		sys_index_stats.[index_id], 
		sys_index_stats.[page_count],
		sys_index_stats.[index_type_desc],
		sys_index_stats.[avg_fragmentation_in_percent],
		sys_index_stats.[avg_fragment_size_in_pages],
		sys_index_stats.[avg_page_space_used_in_percent],
		sys_index_stats.[fragment_count] 
FROM sys.dm_db_index_physical_stats(DB_ID('people_ua_db'), NULL, NULL, NULL, 'SAMPLED') AS sys_index_stats
	INNER JOIN sys.indexes AS sys_index  
	ON sys_index_stats.[object_id] = sys_index.[object_id]
		AND sys_index_stats.[index_id]=sys_index.[index_id]
WHERE object_name(sys_index_stats.[object_id])
	IN ('people');