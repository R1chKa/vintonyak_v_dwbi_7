USE [people_ua_db]
GO

CREATE CLUSTERED INDEX ci_people_id ON people(id)
WITH (fillfactor=50, pad_index=on);

CREATE NONCLUSTERED INDEX ni_people_name ON people(name)
WITH (fillfactor=50, pad_index=on);

CREATE NONCLUSTERED INDEX ni_people_birthday_sex ON people(birth)
INCLUDE(sex);