USE [people_ua_db]
GO

DECLARE @i int,
		@c int
SET @i = 1 + (SELECT COUNT(*) FROM people)
SET @c = (SELECT COUNT(*) FROM people) + 1000000	-- insert  1 000 000  rows

WHILE @i < @c
BEGIN
	DECLARE @sex NCHAR(1)
	SET @sex = 
	(
		SELECT TOP 1 sex
		FROM 
		( 
			VALUES	('w'),
					('m')
		) s(sex)
		ORDER BY NEWID()
	)

	DECLARE @FromDate DATE = '1925-01-01',
			@ToDate DATE = '2000-01-01',
			@birth DATE
	SET @birth = 
	(
		SELECT dateadd(day, rand(checksum(newid()))*(1+datediff(day, @FromDate, @ToDate)), @FromDate)
	)

	DECLARE @region NVARCHAR(30)
	SET @region = 
	(
		SELECT TOP 1 region
		FROM 
		( 
			VALUES	('³������� �������'),
					('��������� �������'),
					('���������������� �������'),
					('�������� �������'),
					('����������� �������'),
					('������������ �������'),
					('��������� �������'),
					('�����-���������� �������'),
					('������� �������'),
					('ʳ������������ �������'),
					('��������� �������'),
					('�������� �������'),
					('����������� �������'),
					('������� �������'),
					('���������� �������'),
					('г�������� �������'),
					('������� �������'),
					('������������ �������'),
					('��������� �������'),
					('���������� �������'),
					('����������� �������'),
					('��������� �������'),
					('����������� �������'),
					('����������� �������')
		) r(region)
		ORDER BY NEWID()
	)


	INSERT INTO people (surname, name, sex, birth, region)
	VALUES
	(
		'Surname - ' + CAST(@i as NVARCHAR(10)),
		'Name - ' + CAST(@i as NVARCHAR(10)),
		@sex,
		@birth,
		@region
	)
	SET @i = @i + 1
END

SELECT * FROM people