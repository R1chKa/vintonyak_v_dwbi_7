USE labor_sql
GO

--1
SELECT ROW_NUMBER() OVER(ORDER BY id_comp, trip_no) AS num, trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no

--2
SELECT ROW_NUMBER() OVER(PARTITION BY id_comp ORDER BY trip_no) AS num, trip_no, id_comp
FROM trip
ORDER BY id_comp, trip_no

--3
SELECT model, color, type, price
FROM (
		 SELECT model, color, type, price, 
			RANK() OVER(PARTITION BY type ORDER BY price) AS price_rank
		 FROM printer
	 ) ss
WHERE price_rank = 1

--4
SELECT maker 
FROM (
		 SELECT maker, RANK() OVER(PARTITION BY maker ORDER BY model) model_rank
		 FROM product 
		 WHERE type = 'pc'
	 ) ss
WHERE model_rank > 2

--5
SELECT DISTINCT price
FROM (
		 SELECT price, DENSE_RANK() OVER(ORDER BY price DESC) AS d_rank
		 FROM pc
	 ) ss
WHERE d_rank = 2

--6
SELECT id_psg, name,
       NTILE(3) OVER(ORDER BY sname_l) AS psg_group 
FROM (
		 SELECT id_psg, name,
			LTRIM(REVERSE(SUBSTRING(LTRIM(REVERSE(name)), 1, CHARINDEX(' ', LTRIM(REVERSE(name)))))) AS sname_l
		 FROM passenger
	 ) ss
ORDER BY sname_l

--7
SELECT code, model, speed, ram, hd, cd, price,
	ROW_NUMBER() OVER(ORDER BY price DESC) AS id,
	COUNT(*) OVER() AS row_total,
	NTILE(CAST((SELECT CEILING(COUNT(*)/3.0) FROM PC) AS bigint)) OVER(ORDER BY price DESC) AS page_num,
	(SELECT CEILING(COUNT(*)/3.0) FROM PC) AS page_total
FROM pc
ORDER BY price DESC

--8
SELECT max_sum, type, date, point
FROM (
		 SELECT MAX(inc) OVER() AS max_sum, *
		 FROM (
			SELECT inc, 'inc' type, date, point
			FROM income

			UNION ALL

			SELECT inc, 'inc' type, date, point
			FROM income_o

			UNION ALL

			SELECT out, 'out' type, date, point
			FROM outcome

			UNION ALL

			SELECT out, 'out' type, date, point
			FROM outcome_o
		 ) ss2
	 ) ss1
WHERE inc = max_sum

--9
SELECT *, avg(price) OVER() AS total_price
FROM (
		 SELECT *, price - AVG(price) OVER() AS dif_total_price 
		 FROM (
				  SELECT *, price - AVG(price) OVER(PARTITION BY speed) AS dif_local_price
				  FROM pc
			  ) ss2
	 ) ss1