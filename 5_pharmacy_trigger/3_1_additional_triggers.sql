USE triger_fk_cursor
GO

DROP TRIGGER IF EXISTS [tr_Employee_check]
GO
CREATE TRIGGER [tr_Employee_check] ON [employee]
AFTER INSERT, UPDATE
AS
IF EXISTS
(
	SELECT inserted.identity_number FROM [employee]
	INNER JOIN inserted ON employee.id = inserted.id
	WHERE inserted.identity_number LIKE '%00'
)
BEGIN
	PRINT 'ERROR: identity_number can''t ending with two zeroes (...00)'
	ROLLBACK TRANSACTION
END
GO

DROP TRIGGER IF EXISTS [tr_Medicine_check]
GO
CREATE TRIGGER [tr_Medicine_check] ON [medicine]
AFTER INSERT, UPDATE
AS
IF EXISTS
(
	SELECT inserted.ministry_code FROM [medicine]
	INNER JOIN inserted ON [medicine].id = inserted.id
	WHERE inserted.ministry_code NOT LIKE '[^MP][^MP]-[0-9][0-9][0-9]-[0-9][0-9]'
)
BEGIN
	PRINT 'ERROR: ministry_code'
	ROLLBACK TRANSACTION
END
GO

DROP TRIGGER IF EXISTS [tr_Post_restriction]
GO
CREATE TRIGGER [tr_Post_restriction] ON [post]
AFTER INSERT, UPDATE
AS
IF EXISTS
(
		SELECT * FROM [post]
		INNER JOIN inserted ON [post].post = inserted.post
)
BEGIN
	PRINT 'ERROR: Inserting or updating table post is forbidden'
	ROLLBACK TRANSACTION
END
GO