USE triger_fk_cursor
GO

DECLARE @Surname VARCHAR(30), @Name VARCHAR(30)
DECLARE @SQLquery1 NVARCHAR(max), @SQLquery2 NVARCHAR(max)
DECLARE @ColumnNum INT;
DECLARE Emp_Cursor CURSOR
	FOR SELECT [Surname], [Name] FROM [employee]

OPEN Emp_Cursor
	FETCH NEXT FROM Emp_Cursor INTO @Surname, @Name
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @SQLquery2 = '';
		SET @ColumnNum = FLOOR(RAND()*(9-1+1)+1); -- random number 1-9
		SET @SQLquery1 = 'CREATE TABLE ' + @Surname + @Name + ' ('
		WHILE @ColumnNum > 0
		BEGIN
			SET @SQLquery2 = @SQLquery2 + ' column' + CONVERT(CHAR(1), @ColumnNum) + ' INT, ';
			SET @ColumnNum = @ColumnNum - 1;
		END
		SET @SQLquery2 = LEFT(@SQLquery2, LEN(@SQLquery2) - 1) -- delete last comma character
		SET @SQLquery2 = @SQLquery2 + ')'
		EXEC(@SQLquery1 + @SQLquery2)
		FETCH NEXT FROM Emp_Cursor INTO @Surname, @Name
	END
CLOSE Emp_Cursor
DEALLOCATE Emp_Cursor