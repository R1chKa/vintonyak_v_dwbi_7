USE triger_fk_cursor
GO

DECLARE @id				 int,
		@surname		 VARCHAR(30),
		@name			 VARCHAR(30),
		@midle_name		 VARCHAR(30),
		@identity_number CHAR(10),
		@passport		 CHAR(10),
		@experience		 DECIMAL(10, 1),
		@birthday		 DATE,
		@post			 VARCHAR(15),
		@pharmacy_id	 INT
DECLARE @SQLquery1 nvarchar(max)
DECLARE @RandomBIT BIT
DECLARE @Num INT, @RowsCount INT
DECLARE @Time VARCHAR(10)
DECLARE Emp_Cursor2 CURSOR
	FOR SELECT  id, surname, name, midle_name, identity_number, 
				passport, experience, birthday, post, pharmacy_id 
	FROM [employee]

OPEN Emp_Cursor2

	SET @Time = (select CONVERT(VARCHAR(10), FORMAT(GETDATE() , 'HH_mm_ss')))
	SET @Num = 2
	SET @RowsCount = (SELECT COUNT(id) FROM [employee])

	WHILE @Num > 0
	BEGIN
		SET @SQLquery1 = 
		'CREATE TABLE [' + CONVERT(CHAR(1), @Num) + '_employee_' + @Time + '] (
			id                 INT,
			surname            VARCHAR(30),
			name               CHAR(30),
			midle_name         VARCHAR(30),
			identity_number    CHAR(10),
			passport           CHAR(10),
			experience         DECIMAL(10, 1),
			birthday           DATE,
			post               VARCHAR(15),
			pharmacy_id        INT)'
		EXEC(@SQLquery1)
		SET @Num = @Num - 1
	END

	FETCH NEXT FROM Emp_Cursor2 
	INTO @id, @Surname, @Name, @midle_name, @identity_number, 
		 @passport, @experience, @birthday, @post, @pharmacy_id

	WHILE @@FETCH_STATUS = 0
	BEGIN

		SET @RandomBIT = CAST(ROUND(RAND(),0) AS BIT) -- random 1 or 0
		IF @RandomBIT = 1
			EXEC('INSERT INTO [1_employee_' + @Time + '] SELECT * FROM employee WHERE id = ' + @id)
		ELSE
			EXEC('INSERT INTO [2_employee_' + @Time + '] SELECT * FROM employee WHERE id = ' + @id)

		FETCH NEXT FROM Emp_Cursor2 
		INTO @id, @Surname, @Name, @midle_name, @identity_number, 
				@passport, @experience, @birthday, @post, @pharmacy_id

	END
CLOSE Emp_Cursor2
DEALLOCATE Emp_Cursor2