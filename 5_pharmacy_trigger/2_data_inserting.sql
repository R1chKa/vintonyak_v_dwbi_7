USE triger_fk_cursor
GO

INSERT INTO [employee] (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)
VALUES ('Aaaaa','Aaaaa','Aaaaa','111111','111aaa',1111,'2001-01-01','post111',1),
	('Bbbbb','Bbbbb','Bbbbb','22222','222bbb',22222,'2002-02-02','post222',2),
	('Ccccc','Ccccc','Ccccc','33333','333ccc',33333,'2003-03-03','post333',3),
	('Ddddd','Ddddd','Ddddd','44444','4444ddd',44444,'2004-04-04','post444',4);

INSERT INTO [medicine] (name, ministry_code, recipe, narcotic, psychotropic)
VALUES ('Zzzz', 'zzzz', 1, 0, 0),
	('Xxxx', 'xxxx', 1, 1, 0),
	('Wwww', 'wwww', 1, 1, 1),
	('Yyyy', 'yyyy', 0, 0, 0);

INSERT INTO [medicine_zone] (medicine_id, zone_id)
VALUES (1, 4),
	(2, 3),
	(3, 2),
	(4, 1);

INSERT INTO [pharmacy] (name, building_number, www, street)
VALUES ('Kkkk', 'k0k0k0', 'www.kkk.com', 'street111'),
	('Nnnn', 'n0n0n0', 'www.nnn.com', 'street222'),
	('Mmmm', 'm0m0m0', 'www.mmm.com', 'street333'),
	('Pppp', 'p0p0p0', 'www.ppp.com', 'street222');

INSERT INTO [pharmacy_medicine] (pharmacy_id, medicine_id)
VALUES (1, 3),
	(2, 4),
	(3, 1),
	(4, 2);

INSERT INTO [post] (post)
VALUES ('post111'),
	('post222'),
	('post333'),
	('post444');

INSERT INTO [street] (street)
VALUES ('street111'),
	('street222'),
	('street333'),
	('street444');

INSERT INTO [zone] (name)
VALUES ('zone111'),
	('zone222'),
	('zone333'),	
	('zone444');

SELECT * FROM [employee]
SELECT * FROM [medicine]
SELECT * FROM [medicine_zone]
SELECT * FROM [pharmacy]
SELECT * FROM [pharmacy_medicine]
SELECT * FROM [post]
SELECT * FROM [street]
SELECT * FROM [zone]