USE triger_fk_cursor
GO

---- Triggers for [post] - [employee] relation
	-- Trigger on side ONE to prevent DELETE, ON UPDATE CASCADE
	DROP TRIGGER IF EXISTS [tr_FK_Post_Emloyees1]
	GO
	CREATE TRIGGER [tr_FK_Post_Emloyees1] ON [post]
	AFTER DELETE, UPDATE
	AS
	BEGIN
		IF @@ROWCOUNT = 0 RETURN
			DECLARE @operation CHAR(1)
			DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
			DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

		SET @operation = 
		CASE	 
			WHEN @ins > 0 AND @del = 0 THEN 'I'
			WHEN @ins > 0 AND @del > 0 THEN 'U'
			WHEN @ins = 0 AND @del > 0 THEN 'D'
		END

		IF EXISTS
		(
			SELECT * FROM [employee]
			INNER JOIN deleted ON [employee].post = deleted.post
		)
		AND
		@operation = 'U'
		BEGIN
			ALTER TABLE [employee] DISABLE TRIGGER [tr_FK_Post_Emloyees2]

			UPDATE [employee] SET [employee].post = inserted.post
			FROM inserted where [employee].post in (select deleted.post from deleted)

			ALTER TABLE [employee] ENABLE TRIGGER [tr_FK_Post_Emloyees2]
			select post as ins from inserted
			select post as del from deleted
		END

		IF EXISTS
		(
			SELECT * FROM [employee]
			INNER JOIN deleted ON [employee].post = deleted.post
		)
		AND @operation = 'D'
		BEGIN
			PRINT 'ERROR: FK_Post_Emloyees delete restrict'
			ROLLBACK TRANSACTION
		END
	END
	GO

	-- Trigger on side MANY to prevent INSERT/UPDATE
	DROP TRIGGER IF EXISTS [tr_FK_Post_Emloyees2]
	GO
	CREATE TRIGGER [tr_FK_Post_Emloyees2] ON [employee]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [post]
		INNER JOIN inserted ON [post].post = inserted.post
	)
	BEGIN
		PRINT 'ERROR: FK_Post_Emloyees insert or update restrict'
		ROLLBACK TRANSACTION
	END
	GO

---- Triggers for [street] - [pharmacy] relation
	-- Trigger on side MANY to prevent DELETE, OU UPDATE SET NULL
	DROP TRIGGER IF EXISTS [tr_FK_Street_Pharmacy1]
	GO
	CREATE TRIGGER [tr_FK_Street_Pharmacy1] ON [street]
	AFTER DELETE, UPDATE
	AS
	IF EXISTS
	(
		SELECT * FROM [pharmacy]
		INNER JOIN deleted ON [pharmacy].street = deleted.street
	)
	BEGIN
		PRINT 'ERROR: FK_Street_Pharmacy delete restrict'
		ROLLBACK TRANSACTION
	END
	GO

	-- Trigger on side MANY to prevent INSERT
	DROP TRIGGER IF EXISTS [tr_FK_Street_Pharmacy2]
	GO
	CREATE TRIGGER [tr_FK_Street_Pharmacy2] ON [pharmacy]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [street]
		INNER JOIN inserted ON [street].street = inserted.street
	)
	BEGIN
		PRINT 'ERROR: FK_Street_Pharmacy insert restrict'
		ROLLBACK TRANSACTION
	END
	GO

---- Triggers for [zone] - [medicine_zone] relation
	-- Trigger on side ONE to prevent DELETE 
	DROP TRIGGER IF EXISTS [tr_FK_Zone_MedicineZone1]
	GO
	CREATE TRIGGER [tr_FK_Zone_MedicineZone1] ON [medicine_zone]
	AFTER DELETE 
	AS
	IF EXISTS
	(
		SELECT * FROM [zone]
		INNER JOIN deleted ON [zone].id = deleted.zone_id
	)
	BEGIN
		PRINT 'ERROR: FK_Zone_MedicineZone delete restrict'
		ROLLBACK TRANSACTION
	END
	GO

	-- Trigger on side MANY to prevent INSERT/UPDATE
	DROP TRIGGER IF EXISTS [tr_FK_Zone_MedicineZone2]
	GO
	CREATE TRIGGER [tr_FK_Zone_MedicineZone2] ON [zone]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [medicine_zone]
		INNER JOIN inserted ON [medicine_zone].zone_id = inserted.id
	)
	BEGIN
		PRINT 'ERROR: FK_Zone_MedicineZone insert or update restrict'
		ROLLBACK TRANSACTION
	END
GO

---- Triggers for [medicine] - [medicine_zone] relation
	-- Trigger on side ONE to prevent DELETE 
	DROP TRIGGER IF EXISTS [tr_FK_Medecine_MedicineZone1]
	GO
	CREATE TRIGGER [tr_FK_Medecine_MedicineZone1] ON [medicine_zone]
	AFTER DELETE 
	AS
	IF EXISTS
	(
		SELECT * FROM [medicine]
		INNER JOIN deleted ON [medicine].id = deleted.medicine_id
	)
	BEGIN
		PRINT 'ERROR: FK_Medecine_MedicineZone delete restrict'
		ROLLBACK TRANSACTION
	END
	GO

	-- Trigger on side MANY to prevent INSERT/UPDATE
	DROP TRIGGER IF EXISTS [tr_FK_Medecine_MedicineZone2]
	GO
	CREATE TRIGGER [tr_FK_Medecine_MedicineZone2] ON [medicine]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [medicine_zone]
		INNER JOIN inserted ON [medicine_zone].medicine_id = inserted.id
	)
	BEGIN
		PRINT 'ERROR: FK_Medecine_MedicineZone insert or update restrict'
		ROLLBACK TRANSACTION
	END
GO

---- Triggers for [pharmacy] - [pharmacy_medicine] relation
	-- Trigger on side ONE to prevent DELETE 
	DROP TRIGGER IF EXISTS [tr_FK_Pharmacy_PharmacyMedicine1]
	GO
	CREATE TRIGGER [tr_FK_Pharmacy_PharmacyMedicine1] ON [pharmacy_medicine]
	AFTER DELETE 
	AS
	IF EXISTS
	(
		SELECT * FROM [pharmacy]
		INNER JOIN deleted ON [pharmacy].id = deleted.pharmacy_id
	)
	BEGIN
		PRINT 'ERROR: FK_Pharmacy_PharmacyMedicine delete restrict'
		ROLLBACK TRANSACTION
	END
	GO

	-- Trigger on side MANY to prevent INSERT/UPDATE
	DROP TRIGGER IF EXISTS [tr_FK_Pharmacy_PharmacyMedicine2]
	GO
	CREATE TRIGGER [tr_FK_Pharmacy_PharmacyMedicine2] ON [pharmacy]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [pharmacy_medicine]
		INNER JOIN inserted ON [pharmacy_medicine].pharmacy_id = inserted.id
	)
	BEGIN
		PRINT 'ERROR: FK_Pharmacy_PharmacyMedicine insert or update restrict'
		ROLLBACK TRANSACTION
	END
GO

---- Triggers for [medicine] - [pharmacy_medicine] relation
	-- Trigger on side ONE to prevent DELETE 
	DROP TRIGGER IF EXISTS [tr_FK_Medecine_PharmacyMedicine1]
	GO
	CREATE TRIGGER [tr_FK_Medecine_PharmacyMedicine1] ON [pharmacy_medicine]
	AFTER DELETE 
	AS
	IF EXISTS
	(
		SELECT * FROM [medicine]
		INNER JOIN deleted ON [medicine].id = deleted.medicine_id
	)
	BEGIN
		PRINT 'ERROR: FK_Medecine_PharmacyMedicine delete restrict'
		ROLLBACK TRANSACTION
	END
	GO

	-- Trigger on side MANY to prevent INSERT/UPDATE
	DROP TRIGGER IF EXISTS [tr_FK_Medecine_PharmacyMedicine2]
	GO
	CREATE TRIGGER [tr_FK_Medecine_PharmacyMedicine2] ON [medicine]
	AFTER INSERT, UPDATE
	AS
	IF NOT EXISTS
	(
		SELECT * FROM [pharmacy_medicine]
		INNER JOIN inserted ON [pharmacy_medicine].medicine_id = inserted.id
	)
	BEGIN
		PRINT 'ERROR: FK_Medecine_PharmacyMedicine insert or update restrict'
		SELECT * FROM [pharmacy_medicine]
		INNER JOIN inserted ON [pharmacy_medicine].medicine_id = inserted.id
		select * from inserted
		select * from [pharmacy_medicine]

		ROLLBACK TRANSACTION
	END
GO