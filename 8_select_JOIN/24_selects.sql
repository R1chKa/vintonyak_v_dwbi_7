use [labor_sql];
go

--1
SELECT maker, type, speed, hd
FROM product pr
JOIN pc
	ON pr.model = pc.model
WHERE hd <= 8

--2
SELECT DISTINCT maker
FROM product pr
JOIN pc
	ON pr.model = pc.model
WHERE speed >= 600

--3
SELECT DISTINCT maker
FROM product pr
JOIN laptop lt
	ON pr.model = lt.model
WHERE speed <= 500

--4
SELECT DISTINCT lt1.model, lt2.model, lt1.hd, lt1.ram
FROM laptop lt1, laptop lt2
WHERE lt1.hd = lt2.hd
	  AND lt1.ram = lt2.ram
	  AND lt1.model > lt2.model

--5
SELECT cl1.country, cl1.class AS bb_class, cl2.class AS bc_class
FROM classes cl1, classes cl2
WHERE cl1.country = cl2.country
	  AND cl1.type = 'bb'
	  AND cl2.type = 'bc'

--6
SELECT DISTINCT pr.model, maker
FROM product pr
JOIN pc
	ON pr.model = pc.model
WHERE price < 600

--7
SELECT pr.model, maker
FROM product pr
JOIN printer
	ON pr.model = printer.model
WHERE price > 300

--8
SELECT DISTINCT maker, pr.model, price
FROM product pr
JOIN pc
	ON pr.model = pc.model

--9
SELECT maker, pr.model, price
FROM product pr
JOIN pc
	ON pr.model = pc.model

--10
SELECT maker, type, pr.model, speed
FROM product pr
JOIN laptop lt
	ON pr.model = lt.model
WHERE speed > 600

--11
SELECT name, displacement
FROM ships s
JOIN classes c
	ON s.class = c.class

--12
SELECT ship, name, date
FROM outcomes o
JOIN battles b
	ON o.battle = b.name
WHERE NOT EXISTS (
				 SELECT *
				 FROM outcomes o2
				 WHERE (o2.result='damaged'
						OR o2.result='sunk')
						AND o.ship=o2.ship
				 )

--13
SELECT name, country
FROM ships s
INNER JOIN classes c
	ON s.class = c.class

--14
SELECT DISTINCT plane, name
FROM trip t
INNER JOIN company c
	ON t.id_comp = c.id_comp
WHERE plane = 'Boeing';

--15
SELECT DISTINCT p.id_psg, name, date
FROM passenger p
INNER JOIN pass_in_trip pin
	ON p.id_psg = pin.id_psg;

--16
SELECT pr.model, speed, hd
FROM product pr
JOIN pc
	ON pr.model = pc.model
WHERE (hd = 10 OR hd = 20)
	  AND maker = 'A'
ORDER BY pc.speed

--17
SELECT *
FROM (
		 SELECT  maker,type FROM product
	 ) AS info 
PIVOT (
		  COUNT(type)
		  FOR type IN (pc, laptop, printer)
	  ) AS PVT_table

--18
SELECT *
FROM (
		 SELECT price  AS p_avg, screen FROM laptop
	 ) AS info
PIVOT (
		  AVG(p_avg)
		  FOR screen IN([11], [12], [14], [15])
	  ) AS PVT_table

--19
SELECT pr.maker, lt.*
FROM product pr 
CROSS APPLY (
				SELECT *
				FROM laptop lt
				WHERE pr.model = lt.model
			) lt

--20
SELECT *
FROM laptop lt1
CROSS APPLY (
				SELECT  MAX(price) max_price
				FROM laptop lt2
				JOIN  product pr1
					ON lt2.model = pr1.model
				WHERE maker = (
								  SELECT maker
								  FROM product pr2
								  WHERE pr2.model = lt1.model
							  )
			) s

--21
SELECT *
FROM laptop lt1
CROSS APPLY (
				SELECT TOP 1 * 
				FROM laptop lt2
				WHERE lt1.model < lt2.model
				OR (lt1.model = lt2.model
					AND lt1.code < lt2.code)
				ORDER BY model, code
			) s
ORDER BY lt1.model

--22
SELECT *
FROM laptop lt1 
OUTER APPLY (
				SELECT TOP 1 *
				FROM laptop lt2
				WHERE lt1.model < lt2.model
				OR (lt1.model = lt2.model
					AND lt1.code < lt2.code)
				ORDER BY model, code
			) s
ORDER BY lt1.model

--23
SELECT s.*
FROM (
	 SELECT DISTINCT type
	 FROM product
	 )pr1
CROSS APPLY (
				SELECT TOP 3 *
				FROM product pr2
				WHERE pr1.type = pr2.type
				ORDER BY pr2.model
			) s

--24
SELECT code, name, value
FROM laptop
CROSS APPLY (
				VALUES ('speed', speed),
				('ram', ram),
				('hd', hd),
				('screen', screen)
			)
spec(name, value)
WHERE code < 4 
ORDER BY code, name, value