USE [V_Vintonyak_Library]
GO

UPDATE [Authors] 
SET [birthday] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[book_amount] = DEFAULT, 
	[issue_amount] = DEFAULT, 
	[total_edition] = DEFAULT;

UPDATE [Books] 
SET [title] = '''Some title for book''',
	[edition] = ABS(CHECKSUM(NEWID()) % 10000) + 50, 
	[published] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 10000), '1990-01-01'), 
	[issue] = ABS(CHECKSUM(NEWID()) % 5) + 1;

UPDATE [Publishers] 
SET [created] = DATEADD(DAY, ABS(CHECKSUM(NEWID()) % 20000), '1950-01-01'),
	[country] = 'UKR',
	[City] = '----',
	[book_amount] = DEFAULT,
	[issue_amount] = DEFAULT,
	[total_edition] = DEFAULT;

SELECT * FROM [Authors]
SELECT * FROM [Books]
SELECT * FROM [Publishers]

USE [master]
GO
