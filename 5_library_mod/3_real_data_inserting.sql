USE [V_Vintonyak_Library]
GO

BEGIN TRANSACTION TablesDeleting
	EXEC sp_MSforeachtable "ALTER TABLE ? NOCHECK CONSTRAINT all"

	EXEC sp_MSForEachTable "DELETE FROM Authors"
	EXEC sp_MSForEachTable "DELETE FROM Publishers"
	EXEC sp_MSForEachTable "DELETE FROM Books"
	EXEC sp_MSForEachTable "DELETE FROM BooksAuthors"
	EXEC sp_MSForEachTable "DELETE FROM Authors_log"

	exec sp_MSforeachtable @command1="print '?'", @command2="ALTER TABLE ? WITH CHECK CHECK CONSTRAINT all"
COMMIT TRANSACTION TablesDeleting

--SELECT * FROM [Authors]
INSERT INTO [Authors] ([Author_Id], [Name], [URL], [birthday])
VALUES (1, 'J.K. Rowling', 'www.Rowling.com', '1965-07-31'),
	(2, 'Dan Brown', 'www.Brown.com', '1985-02-4'),
	(3, 'Kevin Kwan', 'www.Kwan.com', '1980-08-21'),
	(4, 'James Buckley', 'www.Buckley.com', '1981-01-14'),
	(5, 'Tommy Orange', 'www.Orange.com', '1981-10-30'),
	(6, 'Clive Cussler', 'www.Cussler.com', '1972-12-05'),
	(7, 'Robin Burcell', 'www.Burcell.com', '1982-11-16'),
	(8, 'Marc Chernoff', 'www.Chernoff.com', '1976-04-11'),
	(9, 'Angel Chernoff', 'www.Chernoff.com', '1970-03-21');
	
--SELECT * FROM [Publishers]
INSERT INTO [Publishers] ([Publisher_Id], [Name], [URL], [created], [country], [City])
VALUES (1,'Bloomsbury','www.bloomsbury.com/', '1986-01-30', 'England', 'London'),
	(2,'Scholastic', 'www.scholastic.com', '1920-10-22', 'United States', 'New-York'),
	(3,'Raincoast','www.raincoast.com', '1979-01-01', 'Canada', 'Vancouver'),
	(4,'Random House Audio','www.randomhouse.com', '1927-01-01', 'United States', 'New-York'),
	(5,'Anchor','www.anchor-publishing.com', '2013-01-01', 'Germany', 'Hamburg'),
	(6,'Penguin Workshop','www.penguin.com', '1935-01-01', 'England', 'London'),
	(7,'Knopf','www.knopfdoubleday.com', '1915-01-01', 'United States', 'London'),
	(8,'G.P. Putnam�s Sons','', '', 'England', 'London'),
	(9,'TarcherPerigee','', '', 'England', 'London');

--SELECT * FROM [Books]
INSERT INTO [Books] ([ISBN], [Publisher_Id], [URL], [Price], [title], [edition], [published], [issue])
VALUES ('0-7475-3269-9', '1', 'www.harrypotter1.com', 25, 'Harry Potter and the Philosopher''s Stone', 300000, '1997-06-26', 1),
	('978-0439708180', '2', 'www.harrypotter1-1.com', 20, 'Harry Potter and the Sorcerer''s Stone', 200000, '1998-01-01', 2),
	('0-7475-3849-2', '1', 'www.harrypotter2.com', 30, 'Harry Potter and the Chamber of Secrets', 600000, '1998-07-02', 1),
	('9780439064866', '2', 'www.harrypotter2-1.com', 25, 'Harry Potter and the Chamber of Secrets', 400000, '1999-06-02', 2),
	('0-7475-4215-5', '1', 'www.harrypotter3.com', 35, 'Harry Potter and the Prisoner of Azkaban', 700000, '1999-07-08', 1),
	('978-0545582933', '2', 'www.harrypotter3-1.com', 30, 'Harry Potter and the Prisoner of Azkaban', 600000, '1999-09-08', 2),
	('0-7475-4624-X', '1', 'www.harrypotter4.com', 40, 'Harry Potter and the Goblet of Fire', 800000, '2000-07-08', 1),
	('9781408855683', '2', 'www.harrypotter4-1.com', 35, 'Harry Potter and the Goblet of Fire', 700000, '2000-07-08', 2),
	('0-7475-5100-6', '1', 'www.harrypotter5.com', 44, 'Harry Potter and the Order of the Phoenix', 2000000, '2003-06-21', 1),
	('978-0439957861', '2', 'www.harrypotter5-1.com', 35, 'Harry Potter and the Order of the Phoenix', 2000000, '2003-06-21', 2),
	('9781551925707', '3', 'www.harrypotter5-2.com', 34, 'Harry Potter and the Order of the Phoenix', 1000000, '2003-06-21', 3),
	('0-7475-8108-8', '1', 'www.harrypotter6.com', 34, 'Harry Potter and the Half-Blood Prince', 5000000, '2005-07-16', 1),
	('9780439784542', '2', 'www.harrypotter6-1.com', 36, 'Harry Potter and the Half-Blood Prince', 4000000, '2005-07-16', 2),
	('0-545-01022-5', '1', 'www.harrypotter7.com', 35, 'Harry Potter and the Deathly Hallows', 3600000, '2007-07-21', 1),
	('9780545010221', '2', 'www.harrypotter7-1.com', 37, 'Harry Potter and the Deathly Hallows', 2500000, '2007-07-21', 2),
	('9781551929767', '3', 'www.harrypotter7-2.com', 27, 'Harry Potter and the Deathly Hallows', 2000000, '2007-07-21', 3),
	('9780739319291', '4', 'www.origin.com', 50, 'Origin', 1000000, '2017-10-03', 1),
	('9780525563761', '5', 'www.crazy-rich-asians.com', 16, 'Crazy Rich Asians', 1434000, '2018-07-17', 1),
	('9780399542619', '6', 'www.who-is-pele.com', 6, 'Who Is Pele?', 140000, '2018-06-05', 1),
	('9780525520375', '7', 'www.there-there.com', 26, 'There There', 500000, '2018-06-05', 1),
	('9780735218734', '8', 'www.the-gray-ghost.com', 29, 'The Gray Ghost', 1234567, '2017-01-01', 1),
	('9780143132776', '9', 'www.getting-back.com', 27, 'Getting Back to Happy', 500000, '2018-05-22', 1);
	--('ISBN', '23', 'SITE', PRICE, 'TITLE', EDITION, 'DATE', ISSUE),

--SELECT * FROM [BooksAuthors]
INSERT INTO [BooksAuthors] ([BooksAuthors_id],[ISBN],[Author_Id],[Seq_No]) 
VALUES(1,'0-7475-3269-9',1,1),
	(2,'978-0439708180',1,2),
	(3,'0-7475-3849-2',1,1),
	(4,'9780439064866',1,2),
	(5,'0-7475-4215-5',1,1),
	(6,'978-0545582933',1,2),
	(7,'0-7475-4624-X',1,1),
	(8,'9781408855683',1,2),
	(9,'0-7475-5100-6',1,1),
	(10,'978-0439957861',1,2),
	(11,'9781551925707',1,3),
	(12,'0-7475-8108-8',1,1),
	(13,'9780439784542',1,2),
	(14,'0-545-01022-5',1,1),
	(15,'9780545010221',1,2),
	(16,'9781551929767',1,3),
	(17,'9780739319291',2,1),
	(18,'9780525563761',3,1),
	(19,'9780399542619',4,1),
	(20,'9780525520375',5,1),
	(21,'9780735218734',6,1),
	(22,'9780735218734',7,1),
	(23,'9780143132776',8,1),
	(24,'9780143132776',9,1);

	SELECT * FROM [Authors]
	SELECT * FROM [Publishers]
	SELECT * FROM [Books]
	SELECT * FROM [BooksAuthors]
	SELECT * FROM [Authors_log]

USE [master]
GO