USE [V_Vintonyak_Library]
GO

ALTER TABLE [Authors]
	ADD [birthday] DATE,
		[book_amount] INT NOT NULL CHECK([book_amount] >= 0) DEFAULT 0,
		[issue_amount] INT NOT NULL CHECK([issue_amount] >= 0) DEFAULT 0,
		[total_edition] INT NOT NULL CHECK([total_edition] >= 0) DEFAULT 0;
		
BEGIN TRANSACTION PKdrop

	ALTER TABLE [BooksAuthors]
		DROP CONSTRAINT [FK_Books_ISBNs]	
	
	-- drop primary key from [Books]
	DECLARE @pkname VARCHAR(40)
	SET @pkname = (SELECT CONSTRAINT_NAME
		FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
		WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsPrimaryKey') = 1
		AND TABLE_NAME = 'Books' AND TABLE_SCHEMA = 'dbo')
	EXEC('ALTER TABLE [Books] DROP CONSTRAINT ' + @pkname)

	ALTER TABLE [Books]
		ADD [title] VARCHAR(60) NOT NULL DEFAULT '''Title''',
			[edition] INT NOT NULL CHECK([edition] >= 1) DEFAULT 1,
			[published] DATE,
			[issue] INT DEFAULT 1,
			--PRIMARY KEY ([ISBN])
			PRIMARY KEY ([ISBN], [issue]);

	ALTER TABLE [BooksAuthors]
		ADD 	CONSTRAINT [FK_Books_ISBNs] FOREIGN KEY ([ISBN], [Seq_No])
		REFERENCES [Books] ([ISBN], [issue])
		ON UPDATE CASCADE		ON DELETE NO ACTION

COMMIT TRANSACTION PKdrop

ALTER TABLE [Publishers]
	ADD [created] DATE NOT NULL DEFAULT '1900-01-01',
		[country] VARCHAR(30) NOT NULL DEFAULT 'USA',
		[City] VARCHAR(30) NOT NULL DEFAULT 'NY',
		[book_amount] INT NOT NULL CHECK([book_amount] >= 0) DEFAULT 0,
		[issue_amount] INT NOT NULL CHECK([issue_amount] >= 0) DEFAULT 0,
		[total_edition] INT NOT NULL CHECK([total_edition] >= 0) DEFAULT 0;

ALTER TABLE [Authors_log]
	ADD [book_amount_old] INT,
		[issue_amount_old] INT,
		[total_edition_old] INT,
		[book_amount_new] INT,
		[issue_amount_new] INT,
		[total_edition_new] INT;

-- drop two unique keys from [BooksAuthors]	
DECLARE @ukname VARCHAR(40)
SET @ukname = (SELECT TOP(1) CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsUniqueCnst') = 1
	AND TABLE_NAME = 'BooksAuthors' AND TABLE_SCHEMA = 'dbo')
EXEC('ALTER TABLE [BooksAuthors] DROP CONSTRAINT ' + @ukname)
DECLARE @ukname1 VARCHAR(40)

SET @ukname1 = (SELECT TOP(1) CONSTRAINT_NAME
	FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
	WHERE OBJECTPROPERTY(OBJECT_ID(CONSTRAINT_SCHEMA + '.' + QUOTENAME(CONSTRAINT_NAME)), 'IsUniqueCnst') = 1
	AND TABLE_NAME = 'BooksAuthors' AND TABLE_SCHEMA = 'dbo')
EXEC('ALTER TABLE [BooksAuthors] DROP CONSTRAINT ' + @ukname1)

USE [master]
GO

USE [V_Vintonyak_Library]
GO

---- main trigger modify
DROP TRIGGER IF EXISTS [tr_LibraryIUD] 
GO
CREATE TRIGGER [tr_LibraryIUD] ON [Authors]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	IF @@ROWCOUNT = 0 RETURN
		DECLARE @operation CHAR(1)
		DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
		DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

	SET @operation = 
	CASE	 
		WHEN @ins > 0 AND @del = 0 THEN 'I'
		WHEN @ins > 0 AND @del > 0 THEN 'U'
		WHEN @ins = 0 AND @del > 0 THEN 'D'
	END

	-- insert
	IF @operation = 'I'
	BEGIN
		INSERT INTO [Authors_log] 
			([Author_Id_new], [Name_new], [URL_new],
			[book_amount_new], [issue_amount_new], [total_edition_new],
			[operation type])
		SELECT 
			inserted.[Author_Id], inserted.[Name], inserted.[URL],
			inserted.[book_amount], inserted.[issue_amount], inserted.[total_edition],
			@operation
		FROM [Authors] 
			INNER JOIN inserted ON [Authors].[Author_Id] = inserted.[Author_Id]
	END

	-- update
	IF @operation = 'U'
	BEGIN
		UPDATE [Authors] SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER 
		FROM [Authors]
			INNER JOIN inserted ON [Authors].[Author_Id] = inserted.[Author_Id]

		INSERT INTO [Authors_log] 
			([Author_Id_new], [Name_new], [URL_new], [book_amount_new],	[issue_amount_new], [total_edition_new],
			[Author_Id_old], [Name_old], [URL_old], [book_amount_old], [issue_amount_old], [total_edition_old], 
			[operation type])
		SELECT 
			inserted.[Author_Id], inserted.[Name], inserted.[URL],
			inserted.[book_amount], inserted.[issue_amount], inserted.[total_edition],
			deleted.[Author_Id], deleted.[Name], deleted.[URL],
			deleted.[book_amount], deleted.[issue_amount], deleted.[total_edition],
			@operation
		FROM [Authors]
			INNER JOIN  inserted ON [Authors].[Author_Id] = inserted.[Author_Id]
			INNER JOIN deleted ON [Authors].[Author_Id] = deleted.[Author_Id]
	END

	-- delete
	IF @operation = 'D'
	BEGIN
		INSERT INTO [Authors_log] 
			([Author_Id_old], [Name_old], [URL_old], 
			[book_amount_old], [issue_amount_old], [total_edition_old],
			[operation type])
		SELECT 
			deleted.[Author_Id], deleted.[Name], deleted.[URL],
			deleted.[book_amount], deleted.[issue_amount], deleted.[total_edition],
			@operation
		FROM [Authors] 
			INNER JOIN deleted ON [Authors].[Author_Id] = deleted.[Author_Id]
	END
END
GO

EXEC sp_configure 'show advanced options', 1;
GO
RECONFIGURE ;
GO
EXEC sp_configure 'nested triggers', 1 ;
GO
RECONFIGURE;
GO

USE [master]
GO