USE [V_Vintonyak_Library]
GO

---- main trigger creation
DROP TRIGGER IF EXISTS [tr_BooksPublisherIUD] 
GO
CREATE TRIGGER [tr_BooksPublisherIUD] ON [Books]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	IF @@ROWCOUNT = 0 RETURN
		DECLARE @operation CHAR(1)
		DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
		DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

	SET @operation = 
	CASE	 
		WHEN @ins > 0 AND @del = 0 THEN 'I'
		WHEN @ins > 0 AND @del > 0 THEN 'U'
		WHEN @ins = 0 AND @del > 0 THEN 'D'
	END

	-- insert
	IF @operation = 'I'
	BEGIN
	-- update [book_amount] on insert
		UPDATE [Publishers]
		SET [Publishers].[book_amount] = [Publishers].[book_amount] + a.cnt 
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [Publisher_ID], COUNT(*) AS cnt
			FROM inserted
			GROUP BY [Publisher_ID]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]
		
	-- update [issue_amount] on insert
		UPDATE [Publishers]
		SET [Publishers].[issue_amount] = [Publishers].[issue_amount] + a.cnt
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [publisher_Id], count(*) as cnt
			FROM inserted 
			GROUP BY [publisher_Id]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]
		
	-- update [total_edition] on insert
		UPDATE [Publishers]
		SET [Publishers].[total_edition] = [Publishers].[total_edition] + a.sme
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [publisher_Id], sum([edition]) as sme
			FROM inserted 
			GROUP BY [publisher_Id]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]
	END

	-- update
	IF @operation = 'U'
	BEGIN
	-- update [book_amount] on update
		UPDATE [Publishers]
		SET [Publishers].[book_amount] = [Publishers].[book_amount] + i.cnt - d.cnt
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [Publisher_ID], COUNT(*) AS cnt
			FROM inserted
			GROUP BY [Publisher_ID]
		) i
		ON [Publishers].[Publisher_ID] = i.[Publisher_ID]
		INNER JOIN  
		(
			SELECT [Publisher_ID], COUNT(*) AS cnt
			FROM deleted
			GROUP BY [Publisher_ID]
		) d
		ON [Publishers].[Publisher_ID] = d.[Publisher_ID]
		
	-- update [issue_amount] on update
		UPDATE [Publishers]
		SET [Publishers].[issue_amount] = [Publishers].[issue_amount] + i.cnt - d.cnt
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [publisher_Id], count(*) as cnt
			FROM inserted
			GROUP BY [publisher_Id]
		) i
		ON [Publishers].[Publisher_ID] = i.[Publisher_ID]
		INNER JOIN  
		(
			SELECT [publisher_Id], count(*) as cnt
			FROM deleted
			GROUP BY [publisher_Id]
		) d
		ON [Publishers].[Publisher_ID] = d.[Publisher_ID]
		
	-- update [total_edition] on update
		UPDATE [Publishers]
		SET [Publishers].[total_edition] = [Publishers].[total_edition] + i.sme - d.sme
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [publisher_Id], sum([edition]) as sme
			FROM inserted 
			GROUP BY [publisher_Id]
		) i
		ON [Publishers].[Publisher_ID] = i.[Publisher_ID]
		INNER JOIN  
		(
			SELECT [publisher_Id], sum([edition]) as sme
			FROM deleted 
			GROUP BY [publisher_Id]
		) d
		ON [Publishers].[Publisher_ID] = d.[Publisher_ID]
	END

	-- delete
	IF @operation = 'D'
	BEGIN
	-- update [book_amount] on delete
		UPDATE [Publishers]
		SET [Publishers].[book_amount] = [Publishers].[book_amount] - a.cnt
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [Publisher_ID], COUNT(*) AS cnt
			FROM deleted
			GROUP BY [Publisher_ID]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]
		
	-- update [issue_amount] on delete
		UPDATE [Publishers]
		SET [Publishers].[issue_amount] = [Publishers].[issue_amount] - a.cnt
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [publisher_Id], count(*) as cnt
			FROM deleted
			GROUP BY [publisher_Id]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]

	-- update [total_edition] on delete
		UPDATE [Publishers]
		SET [Publishers].[total_edition] = [Publishers].[total_edition] - a.sme
		FROM [Publishers]
		INNER JOIN  
		(
			SELECT [Publisher_Id], sum([edition]) as sme
			FROM deleted 
			GROUP BY [Publisher_Id]
		) a
		ON [Publishers].[Publisher_ID] = a.[Publisher_ID]
	END
END
GO

USE [master]
GO