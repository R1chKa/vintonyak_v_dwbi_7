USE [V_Vintonyak_Library]
GO

---- main trigger creation
DROP TRIGGER IF EXISTS [tr_BooksAuthors_AuthorsIUD] 
GO
CREATE TRIGGER [tr_BooksAuthors_AuthorsIUD] ON [BooksAuthors]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	IF @@ROWCOUNT = 0 RETURN
		DECLARE @operation CHAR(1)
		DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
		DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

	SET @operation = 
	CASE	 
		WHEN @ins > 0 AND @del = 0 THEN 'I'
		WHEN @ins > 0 AND @del > 0 THEN 'U'
		WHEN @ins = 0 AND @del > 0 THEN 'D'
	END

	-- insert
	IF @operation = 'I'
	BEGIN
	-- update [book_amount] on insert
		UPDATE [Authors]
		SET [Authors].[book_amount] = [Authors].[book_amount] + a.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
		
	-- update [issue_amount] on insert
		UPDATE [Authors]
		SET [Authors].[issue_amount] = [Authors].[issue_amount] + a.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
		
	-- update [total_edition] on insert
		UPDATE [Authors]
		SET [Authors].[total_edition] = [Authors].[total_edition] + a.sme
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], sum([edition]) AS sme
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
	END

	-- update
	IF @operation = 'U'
	BEGIN
	-- update [book_amount] on update
		UPDATE [Authors]
		SET [Authors].[book_amount] = [Authors].[book_amount] + i.cnt - d.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) i
		ON [Authors].[Author_ID] = i.[Author_ID]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) d
		ON [Authors].[Author_ID] = d.[Author_ID]
		
	-- update [issue_amount] on update
		UPDATE [Authors]
		SET [Authors].[issue_amount] = [Authors].[issue_amount] + i.cnt - d.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) i
		ON [Authors].[Author_ID] = i.[Author_ID]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) d
		ON [Authors].[Author_ID] = d.[Author_ID]
	
	-- update [total_edition] on update
		UPDATE [Authors]
		SET [Authors].[total_edition] = [Authors].[total_edition] + i.sme
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], sum([edition]) AS sme
			FROM inserted
			INNER JOIN [Books]
			ON inserted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) i
		ON [Authors].[Author_ID] = i.[Author_ID]
		INNER JOIN  
		(
			SELECT [Author_ID], sum([edition]) AS sme
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) d
		ON [Authors].[Author_ID] = d.[Author_ID]
	END

	-- delete
	IF @operation = 'D'
	BEGIN
	-- update [book_amount] on delete
		UPDATE [Authors]
		SET [Authors].[book_amount] = [Authors].[book_amount] - a.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
		
	-- update [issue_amount] on delete
		UPDATE [Authors]
		SET [Authors].[issue_amount] = [Authors].[issue_amount] - a.cnt
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], COUNT(*) AS cnt
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
	
	-- update [total_edition] on delete
		UPDATE [Authors]
		SET [Authors].[total_edition] = [Authors].[total_edition] - a.sme
		FROM [Authors]
		INNER JOIN  
		(
			SELECT [Author_ID], sum([edition]) AS sme
			FROM deleted
			INNER JOIN [Books]
			ON deleted.[ISBN] = [Books].[ISBN]
			GROUP BY [Author_ID]
		) a
		ON [Authors].[Author_ID] = a.[Author_ID]
	END
END
GO

USE [master]
GO