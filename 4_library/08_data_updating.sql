USE [V_Vintonyak_Library]
GO

UPDATE [Authors] SET [URL] = 'www.jkrowling.com' WHERE [Author_Id] = 1
UPDATE [Authors] SET [URL] = DEFAULT WHERE [Author_Id] > 10

UPDATE [Books] SET [Price] = [Price] * 2 WHERE [Price] <= 5
UPDATE [Books] SET [Price] = [Price] * 1.5 WHERE [Price] > 5

UPDATE [Publishers] SET [URL] = DEFAULT WHERE [Publisher_Id] IN (2,7,9,15,19)
UPDATE [Publishers] SET [URL] = 'WITHOUT URL' WHERE [Publisher_Id] IN (2,7,9,15,19)

UPDATE [BooksAuthors] SET [Seq_No] = DEFAULT WHERE ([BooksAuthors_id] % 2) <> 0

SELECT * FROM [Authors]
SELECT * FROM [Books]
SELECT * FROM [Publishers]
SELECT * FROM [BooksAuthors]
SELECT * FROM [Authors_log]

USE [master]
GO