USE [V_Vintonyak_Library]
GO

INSERT INTO [Authors] ([Author_Id], [Name], [URL])
VALUES (1, 'J.K. Rowling', 'www.Rowling.com'),
	(2, 'Patrick Rothfuss', 'www.Rothfuss.com'),
	(3, 'Brandon Sanderson', 'www.Sanderson.com'),
	(4, 'Sherrilyn Kenyon', 'www.Kenyon.com'),
	(5, 'Cassandra Clare', 'www.Clare.com'),
	(6, 'George R.R. Martin', 'www.Martin.com'),
	(7, 'Quino', 'www.Quino.com'),
	(8, 'Kenneth J. Singleton', 'www.Singleton.com'),
	(9, 'Sarah J. Maas', 'www.Maas.com'),
	(10, 'Rick Riordan', 'www.Riordan.com'),
	(11, 'Jorge Luis Borges', 'www.Borges.com'),
	(12, 'Rainer M. Rilke', 'www.Rilke.com'),
	(13, 'Bill Watterson', 'www.Watterson.com'),
	(14, 'Georg Trakl', 'www.Trakl.com'),
	(15, 'Leigh Bardugo', 'www.Bardugo.com'),
	(16, 'Kristin Hannah', 'www.Hannah.com'),
	(17, 'Charles M. Schulz', 'www.Schulz.com'),
	(18, 'Shane K.P. ONeill', 'www.ONeill.com'),
	(19, 'Jim Butcher', 'www.Butcher.com'),
	(20, 'Brian K. Vaughan', 'www.Vaughan.com');

INSERT INTO [Publishers] ([Publisher_Id],[Name],[URL])
VALUES(1,'Mechelle Salazar','www.Salazar.com'),
	(2,'Gage Sawyer','www.Sawyer.com'),
	(3,'Micah Dillard','www.Dillard.com'),
	(4,'Armand Osborn','www.Osborn.com'),
	(5,'Orlando Atkins','www.Atkins.com'),
	(6,'Yael Mcfarland','www.Mcfarland.com.'),
	(7,'George Warner','www.Warner.com'),
	(8,'Alexander Nixon','www.Nixon.com'),
	(9,'Kenneth Ware','www.Ware.com'),
	(10,'Moses Rodgers','www.Rodgers.com'),
	(11,'Carter Hartman','www.Hartman.com'),
	(12,'Hayden Ryan','www.Ryan.com'),
	(13,'Amaya Sawyer','www.Sawyer.com'),
	(14,'Linus Lawrence','www.Lawrence.com'),
	(15,'Noelle Jacobson','www.Jacobson.com'),
	(16,'Tiger Moreno','www.Moreno.com'),
	(17,'Joan Vaughn','www.Vaughn.com'),
	(18,'Calvin Vance','www.Vance.com'),
	(19,'Basil Paul','www.Paul.com'),
	(20,'Dorian Gibson','www.Gibson.com');

INSERT INTO [Books] ([ISBN],[Publisher_Id],[URL],[Price])
VALUES('16470602-3035',19,'www.ToughguyToughgu.com','2.77'),
('16100329-7288',3,'www.Legend.com','5.44'),
('16270915-3064',11,'www.Relentless.com','4.91'),
('16780205-2253',8,'www.Endgame.com','7.38'),
('16500305-3831',4,'www.Brassed .com','5.76'),
('16500312-4061',14,'www.Happiness.com','4.30'),
('16530727-0420',1,'www.Ramen.com','1.42'),
('16470510-8464',10,'www.HeimaHeima.com','8.83'),
('16410712-7161',18,'www.Islan.com','0.58'),
('16551224-9391',5,'www.Bride.com','0.27'),
('16710928-3239',15,'www.Footprints.com','7.78'),
('16920524-2093',17,'www.George.com','7.71'),
('16321112-3538',17,'www.Tender.com','5.55'),
('16110703-7937',18,'www.Shadowzone.com','6.43'),
('16640410-6178',2,'www.Better.com','8.84'),
('16250223-7924',11,'www.Cross.com','5.37'),
('16450211-5035',14,'www.Smiling.com','7.63'),
('16210225-2455',2,'www.Cherry.com','0.18'),
('16611130-3936',14,'www.Futurama.com','5.38'),
('16610107-6112',18,'www.Inescapable.com','2.05');

INSERT INTO [BooksAuthors]([BooksAuthors_id],[ISBN],[Author_Id],[Seq_No])
VALUES(1,'16500305-3831',19,1),
	(2,'16610107-6112',13,1),
	(3,'16710928-3239',8,1),
	(4,'16640410-6178',6,1),
	(5,'16270915-3064',11,1),
	(6,'16500312-4061',17,1),
	(7,'16530727-0420',9,1),
	(8,'16450211-5035',10,1),
	(9,'16920524-2093',5,1),
	(10,'16100329-7288',20,1),
	(11,'16470602-3035',12,1),
	(12,'16611130-3936',16,1),
	(13,'16551224-9391',1,1),
	(14,'16250223-7924',18,1),
	(15,'16321112-3538',2,1),
	(16,'16410712-7161',15,1),
	(17,'16470510-8464',4,1),
	(18,'16110703-7937',14,1),
	(19,'16780205-2253',3,1),
	(20,'16210225-2455',7,1);

SELECT * FROM [Authors]
SELECT * FROM [Publishers]
SELECT * FROM [Books]
SELECT * FROM [BooksAuthors]
SELECT * FROM [Authors_log]

USE [master]
GO