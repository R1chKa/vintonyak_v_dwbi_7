USE [V_Vintonyak_Library]
GO

---- main trigger creation
DROP TRIGGER IF EXISTS [tr_LibraryIUD] 
GO
CREATE TRIGGER [tr_LibraryIUD] ON [Authors]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
	IF @@ROWCOUNT = 0 RETURN
		DECLARE @operation CHAR(1)
		DECLARE @ins INT = (SELECT COUNT(*) FROM inserted)
		DECLARE @del INT = (SELECT COUNT(*) FROM deleted)

	SET @operation = 
	CASE	 
		WHEN @ins > 0 AND @del = 0 THEN 'I'
		WHEN @ins > 0 AND @del > 0 THEN 'U'
		WHEN @ins = 0 AND @del > 0 THEN 'D'
	END

	-- insert
	IF @operation = 'I'
	BEGIN
		INSERT INTO [Authors_log] 
			([Author_Id_new], [Name_new], [URL_new],
			[operation type])
		SELECT 
			inserted.[Author_Id], inserted.[Name], inserted.[URL],
			@operation
		FROM [Authors] 
			INNER JOIN inserted ON [Authors].[Author_Id] = inserted.[Author_Id]
	END

	-- update
	IF @operation = 'U'
	BEGIN
		UPDATE [Authors] SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER 
		FROM [Authors]
			INNER JOIN inserted ON [Authors].[Author_Id] = inserted.[Author_Id]

		INSERT INTO [Authors_log] 
			([Author_Id_new], [Name_new], [URL_new],
			[Author_Id_old], [Name_old], [URL_old],
			[operation type])
		SELECT 
			inserted.[Author_Id], inserted.[Name], inserted.[URL],
			deleted.[Author_Id], deleted.[Name], deleted.[URL],
			@operation
		FROM [Authors]
			INNER JOIN  inserted ON [Authors].[Author_Id] = inserted.[Author_Id]
			INNER JOIN deleted ON [Authors].[Author_Id] = deleted.[Author_Id]
	END

	-- delete
	IF @operation = 'D'
	BEGIN
		INSERT INTO [Authors_log] 
			([Author_Id_old], [Name_old], [URL_old],
			[operation type])
		SELECT 
			deleted.[Author_Id], deleted.[Name], deleted.[URL],
			@operation
		FROM [Authors] 
			INNER JOIN deleted ON [Authors].[Author_Id] = deleted.[Author_Id]
	END
END
GO


---- additional triggers creation
DROP TRIGGER IF EXISTS [tr_BooksU] 
GO
CREATE TRIGGER [tr_BooksU] ON [Books]
AFTER UPDATE
AS
BEGIN
	UPDATE [Books] SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
		FROM [Books]
			INNER JOIN inserted ON [Books].[ISBN] = inserted.[ISBN]
END
GO

DROP TRIGGER IF EXISTS [tr_PublishersU] 
GO
CREATE TRIGGER [tr_PublishersU] ON [Publishers]
AFTER UPDATE
AS
BEGIN
	UPDATE [Publishers] SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
		FROM [Publishers]
			INNER JOIN inserted ON [Publishers].[Publisher_Id] = inserted.[Publisher_Id]
END
GO

DROP TRIGGER IF EXISTS [tr_BooksAuthorsU] 
GO
CREATE TRIGGER [tr_BooksAuthorsU] ON [BooksAuthors]
AFTER UPDATE
AS
BEGIN
	UPDATE [BooksAuthors] SET [updated] = GETDATE(), [updated_by] = SYSTEM_USER
		FROM [BooksAuthors]
			INNER JOIN inserted ON [BooksAuthors].[BooksAuthors_id] = inserted.[BooksAuthors_id]
END
GO

USE [master]
GO