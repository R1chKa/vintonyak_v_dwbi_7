USE [V_Vintonyak_Library]
GO

DELETE TOP (5) FROM [BooksAuthors]
DELETE [Authors] FROM [Authors] LEFT JOIN [BooksAuthors] ON [Authors].[Author_Id] = [BooksAuthors].[Author_Id] WHERE [BooksAuthors_id] IS NULL
DELETE [Books] FROM [Books] LEFT JOIN [BooksAuthors] ON [Books].[ISBN] = [BooksAuthors].[ISBN] WHERE [BooksAuthors].[ISBN] IS NULL
DELETE [Publishers] FROM [Publishers] LEFT JOIN [Books] ON [Publishers].[Publisher_Id] = [Books].[Publisher_Id] WHERE [Books].[Publisher_Id] IS NULL
GO

DISABLE TRIGGER [tr_Authors_logD] ON [Authors_log];  
GO

DELETE TOP(5) FROM [Authors_log]
GO

ENABLE Trigger [tr_Authors_logD] ON [Authors_log];  
GO  

SELECT * FROM [Authors]
SELECT * FROM [Books]
SELECT * FROM [Publishers]
SELECT * FROM [BooksAuthors]
SELECT * FROM [Authors_log]

USE [master]
GO