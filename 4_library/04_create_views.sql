USE [V_Vintonyak_Library_view]
GO

-- Views creation
DROP VIEW IF EXISTS [vw_Authors]
GO
CREATE OR ALTER VIEW [vw_Authors] (
	[Author_Id],
	[Name],
	[URL],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
)
AS
SELECT * FROM [V_Vintonyak_Library].[dbo].[Authors]
GO

DROP VIEW IF EXISTS [vw_Publishers]
GO
CREATE OR ALTER VIEW [vw_Publishers] (
	[Publisher_Id],
	[Name],
	[URL],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
)
AS
SELECT * FROM [V_Vintonyak_Library].[dbo].[Publishers]
GO

DROP VIEW IF EXISTS [vw_Books]
GO
CREATE OR ALTER VIEW [vw_Books] (
	[ISBN],
	[Publisher_Id],
	[URL],
	[Price],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
)
AS
SELECT * FROM [V_Vintonyak_Library].[dbo].[Books]
GO

DROP VIEW IF EXISTS [vw_BooksAuthors]
GO
CREATE OR ALTER VIEW [vw_BooksAuthors] (
	[BooksAuthors_id],
	[ISBN],
	[Author_Id],
	[Seq_No],
	[inserted],
	[inserted_by],
	[updated],
	[updated_by]
)
AS
SELECT * FROM [V_Vintonyak_Library].[dbo].[BooksAuthors]
GO

DROP VIEW IF EXISTS [vw_Authors_log]
GO
CREATE OR ALTER VIEW [vw_Authors_log] (
	[operation_id],
	[Author_Id_new],
	[Name_new],
	[URL_new],
	[Author_Id_old],
	[Name_old],
	[URL_old],
	[operation type],
	[operation_datetime]
)
AS
SELECT * FROM [V_Vintonyak_Library].[dbo].[Authors_log]
GO

USE [master]
GO