USE [V_Vintonyak_Library]
GO

DROP TRIGGER IF EXISTS [tr_Authors_logD]
GO

CREATE TRIGGER [tr_Authors_logD] ON [Authors_log]
AFTER DELETE
AS
IF EXISTS (SELECT * FROM [Authors_log])
BEGIN
	PRINT 'ERROR: Delete restrict'
	ROLLBACK TRANSACTION
END
GO

USE [master]
GO