USE [V_Vintonyak_Library_synonym]
GO

DROP SYNONYM IF EXISTS [sm_Authors]
GO
CREATE SYNONYM [sm_Authors] FOR [V_Vintonyak_Library].[dbo].[Authors]
GO

DROP SYNONYM IF EXISTS [sm_Publishers]
GO
CREATE SYNONYM [sm_Publishers] FOR [V_Vintonyak_Library].[dbo].[Publishers]
GO

DROP SYNONYM IF EXISTS [sm_Books]
GO
CREATE SYNONYM [sm_Books] FOR [V_Vintonyak_Library].[dbo].[Books]
GO

DROP SYNONYM IF EXISTS [sm_BooksAuthors]
GO
CREATE SYNONYM [sm_BooksAuthors] FOR [V_Vintonyak_Library].[dbo].[BooksAuthors]
GO

DROP SYNONYM IF EXISTS [sm_Authors_log]
GO
CREATE SYNONYM [sm_Authors_log] FOR [V_Vintonyak_Library].[dbo].[Authors_log]
GO

USE [master]
GO