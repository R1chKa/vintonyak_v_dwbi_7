USE [V_Vintonyak_Library]
GO

DROP TABLE IF EXISTS [Authors]
GO
CREATE TABLE [Authors] (
	[Author_Id] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR(20) NOT NULL UNIQUE,
	[URL] VARCHAR(40) NOT NULL DEFAULT 'www.author_name.com',
	[inserted] DATETIME NOT NULL DEFAULT GETDATE(),
	[inserted_by] VARCHAR(35) NOT NULL DEFAULT system_user,
	[updated] DATETIME,
	[updated_by] VARCHAR(35)
)

DROP TABLE IF EXISTS [Publishers]
GO
CREATE TABLE [Publishers] (
	[Publisher_Id] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR(20) NOT NULL UNIQUE,
	[URL] VARCHAR(40) NOT NULL DEFAULT 'www.publisher_name.com',
	[inserted] DATETIME NOT NULL DEFAULT GETDATE(),
	[inserted_by] VARCHAR(35) NOT NULL DEFAULT system_user,
	[updated] DATETIME,
	[updated_by] VARCHAR(35)
)

DROP TABLE IF EXISTS [Books]
GO
CREATE TABLE [Books] (
	[ISBN] CHAR(15) NOT NULL PRIMARY KEY,
	[Publisher_Id] INT NOT NULL ,
	[URL] VARCHAR(40) NOT NULL UNIQUE,
	[Price] DECIMAL(6,2) NOT NULL CHECK([Price] >= 0) DEFAULT 0,
	[inserted] DATETIME NOT NULL DEFAULT GETDATE(),
	[inserted_by] VARCHAR(35) NOT NULL DEFAULT system_user,
	[updated] DATETIME,
	[updated_by] VARCHAR(35),

	CONSTRAINT [FK_Books_Publishers] FOREIGN KEY ([Publisher_Id])
	REFERENCES [Publishers] ([Publisher_Id])
)

DROP TABLE IF EXISTS [BooksAuthors]
GO
CREATE TABLE [BooksAuthors] (
	[BooksAuthors_id] INT NOT NULL PRIMARY KEY CHECK([BooksAuthors_id] >= 1) DEFAULT 1,
	[ISBN] CHAR(15) NOT NULL UNIQUE,
	[Author_Id] INT NOT NULL UNIQUE,
	[Seq_No] INT NOT NULL CHECK([Seq_No] >= 1) DEFAULT 1,
	[inserted] DATETIME NOT NULL DEFAULT GETDATE(),
	[inserted_by] VARCHAR(35) NOT NULL DEFAULT system_user,
	[updated] DATETIME,
	[updated_by] VARCHAR(35),

	CONSTRAINT [FK_Books_ISBNs] FOREIGN KEY ([ISBN])
	REFERENCES [Books] ([ISBN])
	ON UPDATE CASCADE		ON DELETE NO ACTION,

	CONSTRAINT [FK_Books_Authors] FOREIGN KEY ([Author_Id])
	REFERENCES [Authors] ([Author_Id])
	ON UPDATE NO ACTION		ON DELETE NO ACTION
)

DROP TABLE IF EXISTS [Authors_log]
GO
CREATE TABLE [Authors_log] (
	[operation_id] INT NOT NULL IDENTITY PRIMARY KEY,
	[Author_Id_new] INT,
	[Name_new] VARCHAR(20),
	[URL_new] VARCHAR(40),
	[Author_Id_old] INT,
	[Name_old] VARCHAR(20),
	[URL_old] VARCHAR(40),
	[operation type] CHAR(1) NOT NULL CHECK([operation type] IN('I','D','U')),
	[operation_datetime] DATETIME NOT NULL DEFAULT GETDATE()
)

USE [master]
GO