USE labor_sql
GO

--1
;WITH CTE_multiple_count (t_name, row_count) AS
(
	SELECT 'product', COUNT(*) AS '������� �����' FROM product
	UNION
	SELECT 'pc', COUNT(*) AS '������� �����' FROM pc
	UNION
	SELECT 'laptop', COUNT(*) AS '������� �����' FROM laptop
	UNION
	SELECT 'printer', COUNT(*) AS '������� �����' FROM printer
)
SELECT *
FROM CTE_multiple_count
ORDER BY 2

--2
;WITH CTE_2 AS
(
	SELECT * FROM product
),
CTE_1 AS 
(
	SELECT * FROM CTE_2 WHERE maker = 'A'
)
SELECT *
FROM CTE_1

--3
;WITH first_level_regions(region_id, place_id, name, place_lvl) AS 
(
	SELECT region_id, id, name, 0
	FROM geography
	WHERE region_id IS NULL

	UNION ALL

	SELECT g.region_id, g.id, g.name, f.place_lvl + 1
	FROM geography g
	INNER JOIN first_level_regions f
		ON g.region_id = f.place_id
)
SELECT region_id, place_id, name, place_lvl
FROM first_level_regions
WHERE place_lvl = 1

--4
;WITH first_level_regions(region_id, place_id, name, place_lvl) AS 
(
	SELECT region_id, id, name, 0
	FROM geography
	WHERE region_id = 4

	UNION ALL

	SELECT g.region_id, g.id, g.name, f.place_lvl + 1
	FROM geography g
	INNER JOIN first_level_regions f
		ON g.region_id = f.place_id
)
SELECT region_id, place_id, name, place_lvl
FROM first_level_regions

--5
;WITH rCTE(i) AS
(
	SELECT i = 1

	UNION ALL

	SELECT i + 1
	FROM rCTE
	WHERE i < 10000
)
SELECT * FROM rCTE
OPTION (maxrecursion 10000)

--6
;WITH rCTE(i) AS
(
	SELECT i = 1

	UNION ALL

	SELECT i + 1
	FROM rCTE
	WHERE i < 100000
)
SELECT * FROM rCTE
OPTION (maxrecursion 0)

--7
;WITH CTE_date AS
(
    SELECT  CAST('01/JAN/2018' AS DATE) as [days]
    UNION ALL
    SELECT DATEADD(DAY, 1, [days]) [days]
    FROM    CTE_date
    WHERE   [days] < CAST('31/DEC/2018' AS DATE)
)
SELECT DISTINCT DATENAME(WEEKDAY, [days]) WD,
				COUNT([days]) OVER(PARTITION BY DATENAME(WEEKDAY, [days])) cnt
FROM CTE_date 
WHERE DATENAME(WEEKDAY, [days]) = 'Saturday'
   OR DATENAME(WEEKDAY, [days]) = 'Sunday'
ORDER BY DATENAME(WEEKDAY, [days])
OPTION (maxrecursion 366)

--8
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND maker NOT IN (
						 SELECT maker
						 FROM product
						 WHERE type = 'laptop'
					 )

--9
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND maker != ALL (
						 SELECT maker
						 FROM product
						 WHERE type = 'laptop'
					 )

--10
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND NOT maker = ANY (
							SELECT maker
							FROM product
							WHERE type = 'laptop'
						)

--11
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND maker IN (
					 SELECT maker
					 FROM product
					 WHERE type = 'laptop'
				 )

--12
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND NOT maker != ALL (
							 SELECT maker
							 FROM product
							 WHERE type = 'laptop'
						 )

--13
SELECT DISTINCT maker
FROM product
WHERE type = 'pc'
	AND maker = ANY (
						SELECT maker
						FROM product
						WHERE type = 'laptop'
					)

--14
SELECT DISTINCT pr.maker
FROM product pr
WHERE pr.type = 'PC'
	AND maker = ALL (
					 SELECT maker
					 FROM pc
					 WHERE pr.model = model
				 )

EXCEPT

SELECT DISTINCT pr.maker
FROM product pr
WHERE pr.type = 'PC'
	AND maker != ALL (
						 SELECT maker
						 FROM pc
						 WHERE pr.model = model
					 )

--15
SELECT c.country, c.class
FROM classes c
WHERE c.country = 'Ukraine' 
	AND EXISTS (
				   SELECT c.country, c.class
				   FROM classes c
				   WHERE c.country = 'Ukraine'
			   )

UNION ALL

SELECT c.country, c.class
FROM classes c
WHERE NOT EXISTS (
					 SELECT c.country, c.class
					 FROM classes c
					 WHERE c.country = 'Ukraine'
				 )

--16
;WITH CTE_ships AS
(
	SELECT o.ship, b.name, b.date, o.result
	FROM outcomes o
	LEFT JOIN battles b
		ON o.battle = b.name
)
SELECT DISTINCT ship, name, date FROM CTE_ships o
WHERE ship IN (
				  SELECT ship 
				  FROM CTE_ships
				  WHERE date < o.date 
					  AND result = 'damaged'
			  )

--17
SELECT DISTINCT pr.maker
FROM product pr
WHERE pr.type = 'PC'
	AND EXISTS (
				   SELECT maker
				   FROM pc
				   WHERE pr.model = model
			   )

EXCEPT

SELECT DISTINCT pr.maker
FROM product pr
WHERE pr.type = 'PC'
	AND NOT EXISTS (
					   SELECT maker
					   FROM pc
					   WHERE pr.model = model
				   )

--18
SELECT DISTINCT maker
FROM product
WHERE [type]='Printer' 
	AND maker IN (
					 SELECT maker
					 FROM product
					 WHERE model IN (
										SELECT model FROM PC
										WHERE speed = (
														  SELECT MAX(speed)
														  FROM PC
													  )
									)
				  )

--19
SELECT cl.class
FROM classes cl
LEFT JOIN ships s
	ON s.class = cl.class
WHERE cl.class IN (
					  SELECT ship
					  FROM outcomes
					  WHERE result = 'sunk'
				  )
	OR s.name IN (
					 SELECT ship
					 FROM outcomes
					 WHERE result = 'sunk'
				 )

--20
SELECT model, price
FROM printer
WHERE price = (
				  SELECT MAX(price)
				  FROM printer
			  )

--21
SELECT (
		   SELECT type
		   FROM product
		   WHERE model = l.model
	   ), 
	   model, speed
FROM laptop l
WHERE speed < (
				  SELECT MIN(speed)
				  FROM pc
			  )

--22
SELECT (
		   SELECT maker
		   FROM product
		   WHERE model = p.model
	   ), price
FROM printer p
WHERE color = 'y'
	AND price = (
					SELECT MIN(price)
					FROM printer
					WHERE color='y'
				)

--23
SELECT DISTINCT o.battle
FROM outcomes o
LEFT JOIN ships s
	ON s.name = o.ship
LEFT JOIN classes c
	ON o.ship = c.class
		OR s.class = c.class
WHERE c.country IS NOT NULL
GROUP BY c.country, o.battle
HAVING COUNT(o.ship) >= 2

--24
SELECT DISTINCT maker, 
       (
		   SELECT COUNT(*) 
		   FROM pc 
		   WHERE model IN (
							  SELECT model 
							  FROM product 
							  WHERE maker=p.maker
						  )
	   ) AS pc,
	   (
		   SELECT COUNT(*) 
		   FROM laptop 
		   WHERE model IN (
							  SELECT model 
							  FROM product
							  WHERE maker=p.maker
						  )
	   ) AS laptop,
	   (
		   SELECT COUNT(*) 
		   FROM printer 
		   WHERE model IN (
							  SELECT model 
							  FROM product 
							  WHERE maker=p.maker
						  )
	   ) AS printer
FROM product p

--25
;WITH CTE_count AS
(
	SELECT DISTINCT maker,
		(
			SELECT COUNT(*) 
			FROM pc 
			WHERE model IN (
							   SELECT model 
							   FROM product 
							   WHERE maker = p.maker
						   )
		) AS pc
	FROM product p
)
SELECT maker,
       CASE
	      WHEN pc > 0 THEN 'yes(' +CAST(pc AS VARCHAR(10)) + ')'
		  ELSE 'no'
	   END AS pc  
FROM CTE_count;

--26
SELECT i.point, i.date, inc, out
FROM income_o i 
LEFT JOIN outcome_o o 
	ON i.point = o.point
		AND i.date = o.date

UNION

SELECT o.point, o.date, inc, out
FROM income_o i
RIGHT JOIN outcome_o o
	ON i.point = o.point
		AND i.date = o.date

--27
SELECT name, numGuns, bore, displacement, type, country, launched, s.class
FROM ships s 
JOIN classes c
	ON s.class = c.class
WHERE
	CASE 
		WHEN numGuns = 8 THEN 1
		ELSE 0
	END +
	CASE 
		WHEN bore = 15 THEN 1 
		ELSE 0 
	END +
	CASE 
		WHEN displacement = 32000 THEN 1 
		ELSE 0 
	END +
	CASE 
		WHEN type = 'bb' THEN 1 
		ELSE 0 
	END +
	CASE 
		WHEN country = 'USA' THEN 1 
		ELSE 0 
	END +
	CASE 
		WHEN launched = 1915 THEN 1 
		ELSE 0 
	END +
	CASE 
		WHEN s.class = 'Kongo' THEN 1 
		ELSE 0 
	END > = 4

--28
;WITH CTE_p_d AS
(
	SELECT point, date
	FROM outcome_o
  
	UNION

	SELECT point, date
	FROM outcome
),

CTE_o AS
	(
	SELECT point, date, SUM(out) AS out 
	FROM outcome
	GROUP BY point, date
),

CTE_o_from_join AS
(
	SELECT cpd.point, cpd.date, COALESCE(o.out, 0) once, COALESCE(co.out, 0) more
	FROM CTE_p_d cpd
	LEFT JOIN outcome_o o
		ON cpd.point = o.point 
			AND cpd.date = o.date
	LEFT JOIN CTE_o co
		ON cpd.point = co.point
			AND cpd.date = co.date
)
SELECT point, date,
	CASE
		WHEN once > more THEN 'once a day'
		WHEN once < more THEN 'more than once a day'
		ELSE 'both'
	END AS lead
FROM CTE_o_from_join

--29
;WITH CTE_products AS
(
	SELECT p.maker, p.model, p.type, NULL as price
	FROM product AS p
)
SELECT cp.maker, cp.model, cp.type, pc.price
FROM CTE_products cp
INNER JOIN pc
	ON cp.model=pc.model 
		AND cp.maker='B'

UNION ALL

SELECT cp.maker, cp.model, cp.type, pr.price
FROM CTE_products cp
INNER JOIN printer pr
	ON cp.model=pr.model 
		AND cp.maker='B'

	UNION ALL

SELECT cp.maker, cp.model, cp.type, l.price
FROM CTE_products cp
INNER JOIN laptop l
	ON cp.model=l.model 
		AND cp.maker='B'

--30
SELECT name, class
FROM (
		SELECT name
		FROM ships
		
		UNION
		
		SELECT ship
		FROM outcomes
	 ) s
INNER JOIN classes 
ON s.name = classes.class

--31
SELECT class
FROM (
		 SELECT class
		 FROM ships

		 UNION ALL

		 SELECT class 
		 FROM outcomes
		 INNER JOIN classes 
			ON classes.class = outcomes.ship
	 ) a
GROUP BY class
HAVING COUNT(class) = 1

--32
SELECT name
FROM ships
WHERE launched < 1942

UNION

SELECT o.ship
FROM outcomes o
INNER JOIN battles b
	ON o.battle = b.name
WHERE b.date < '1942'